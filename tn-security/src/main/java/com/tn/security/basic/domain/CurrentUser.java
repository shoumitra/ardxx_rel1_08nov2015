package com.tn.security.basic.domain;

import java.util.List;

import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User
{

	private User user;

	public CurrentUser(User user)
	{
		super(user.getEmail(), user.getPasswordHash(), AuthorityUtils.createAuthorityList(getRoleNames(user.getRoles())));
		this.user = user;
	}

	public User getUser()
	{
		return user;
	}

	public Long getId()
	{
		return user.getId();
	}

	public List<Role> getRole()
	{
		return user.getRoles();
	}

	private static String[] getRoleNames(List<Role> roleList)
	{
		String[] roles = new String[roleList.size()];
		for (int i = 0; i < roleList.size(); i++)
		{
			roles[i] = roleList.get(i).getRoleName();
		}
		return roles;
	}

	@Override
	public String toString()
	{
		return "CurrentUser{" + "user=" + user + "} " + super.toString();
	}
}
