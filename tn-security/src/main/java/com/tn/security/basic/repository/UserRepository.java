package com.tn.security.basic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tn.security.basic.controller.UserStatus;
import com.tn.security.basic.domain.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>
{

	Optional<User> findOneByEmailAndStatus(String email,UserStatus status);

	public List<User> findByStatusAndRolesId(@Param("status") UserStatus status, @Param("id") Long id);

	@Query(value="select * from user where status = 'ACTIVE' and id in (select user_id from user_role where role_id in(:id))",nativeQuery=true)
	public List<User> findByStatusAndRolesIdIn(@Param("id") List<Long> id);
	
	public List<User> findByStatusAndRolesRoleName(@Param("status") UserStatus status, @Param("roleName") String roleName);
	
	public User findOneByEmail(@Param("email") String email);
	
	public List<User> findByStatusNot(@Param("status") UserStatus status, Sort sort);
	
	
}
