package com.tn.security.basic.controller;

/**
 * 
 * @author Raj
 *
 */
public enum UserStatus
{
	ACTIVE, INACTIVE, DELETED
}
