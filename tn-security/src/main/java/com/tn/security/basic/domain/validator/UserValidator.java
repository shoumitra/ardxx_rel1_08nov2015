package com.tn.security.basic.domain.validator;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.tn.security.basic.dto.UserDTO;
import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;

/**
 * Validator class to validate User values
 * @author Raj
 *
 */
public class UserValidator
{
	/**
	 * Method to validate create user input values
	 * @param userDTO
	 * @return Returns list of error codes
	 */
	public List<String> validateCreateUser(UserDTO userDTO)
	{
		List<String> errorCode = new ArrayList<String>();

		if (TNUtils.isEmpty(userDTO.getEmail()))
		{
			errorCode.add(Messages.properties.getProperty("email.is.empty"));
		}
		else if(!isValidEmail(userDTO.getEmail()))
		{
			errorCode.add(Messages.properties.getProperty("email.invalid"));
		}
		
		if(!TNUtils.isEmpty(userDTO.getPhoneNumber()) && !isNumeric(userDTO.getPhoneNumber()))
		{
			errorCode.add(Messages.properties.getProperty("phonenumber.invalid"));
		}
		
		if (TNUtils.isEmpty(userDTO.getPasswordHash()))
		{
			errorCode.add(Messages.properties.getProperty("password.is.empty"));
		}

		if (userDTO.getStatus() == null | (userDTO.getStatus() != null && TNUtils.isEmpty(userDTO.getStatus().name())))
		{
			errorCode.add(Messages.properties.getProperty("status.is.empty"));
		}

		if (userDTO.getRoleIds() == null | (userDTO.getRoleIds() != null && userDTO.getRoleIds().isEmpty()))
		{
			errorCode.add(Messages.properties.getProperty("role.not.assigned"));
		}

		return errorCode;
	}
	
	public static boolean isNumeric(String value)
	{
		return value.matches("-?\\d+(\\.\\d+)?");
	}
	
	public static boolean isValidEmail(String email)
	{
		try
		{
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
		} catch (AddressException e)
		{
			return false;
		}
		return true;
	}
	
}
