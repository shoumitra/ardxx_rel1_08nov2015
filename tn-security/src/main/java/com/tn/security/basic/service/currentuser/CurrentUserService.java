package com.tn.security.basic.service.currentuser;

import com.tn.security.basic.domain.CurrentUser;
import com.tn.security.basic.dto.UserDTO;

public interface CurrentUserService
{

	boolean canAccessUser(CurrentUser currentUser, Long userId);

	boolean canChangeStatus(CurrentUser currentUser, UserDTO userDTO);

}
