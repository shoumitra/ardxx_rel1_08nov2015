package com.tn.security.service.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tn.security.basic.controller.UserStatus;
import com.tn.security.basic.domain.Role;
import com.tn.security.basic.domain.User;
import com.tn.security.basic.dto.UserDTO;
import com.tn.security.basic.repository.RoleRepository;
import com.tn.security.basic.repository.UserRepository;
import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;
import com.tn.security.exception.AppException;

@Service
public class UserServiceImpl implements UserService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Override
	public Optional<User> getUserById(long id)
	{
		LOGGER.debug("Getting user={}", id);
		return Optional.ofNullable(userRepository.findOne(id));
	}

	@Override
	public Optional<User> getUserByEmail(String email)
	{
		LOGGER.debug("Getting user by email={}", email.replaceFirst("@.*", "@***"));
		return userRepository.findOneByEmailAndStatus(email, UserStatus.ACTIVE);
	}

	@Override
	public Collection<UserDTO> getAllUsers()
	{
//		Pageable pageable = new PageRequest(page, size,Direction.ASC,"email");
		
		Iterable<User> users = userRepository.findByStatusNot(UserStatus.DELETED,new Sort("email"));
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (User user : users)
		{
			UserDTO userDTO = new UserDTO(user);
			userDTOs.add(userDTO);
		}

		return userDTOs;
	}

	@Override
	public List<UserDTO> getUsersByRole(Long roleId)
	{
		List<User> users = userRepository.findByStatusAndRolesId(UserStatus.ACTIVE, roleId);
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (User user : users)
		{
			UserDTO userDTO = new UserDTO(user);
			userDTOs.add(userDTO);
		}

		return userDTOs;
	}

	@Override
	public UserDTO getUser(Long userId)
	{
		User user = userRepository.findOne(userId);
		UserDTO userDTO = new UserDTO(user);
		return userDTO;
	}

	@Override
	public UserDTO createUser(UserDTO userDTO) throws Exception
	{
		User user = userRepository.findOneByEmail(userDTO.getEmail());
		if (user == null)
		{
			user = new User();
			UserDTO userDTOAfterSave = prepareUser(userDTO, user);
			return userDTOAfterSave;
		} else
		{
			throw new AppException(Messages.properties.getProperty("user.email.exists"));
		}

	}

	@Override
	public UserDTO updateUser(UserDTO userDTO)
	{
		User user = userRepository.findOne(userDTO.getId());
		
		if(userDTO.getStatus() == null)
		{
			userDTO.setStatus(user.getStatus());
		}
		
		UserDTO userDTOAfterSave = prepareUser(userDTO, user);
		return userDTOAfterSave;
	}

	private UserDTO prepareUser(UserDTO userDTO, User user)
	{
		user = userDTO.toDomain(user);

		if (userDTO.getPasswordHash() != null && !userDTO.getPasswordHash().isEmpty())
		{
			user.setPasswordHash(new BCryptPasswordEncoder().encode(userDTO.getPasswordHash()));
		}

		List<Role> roles = new ArrayList<Role>();
		Role role = null;
		if (userDTO.isExternalUser())
		{
			role = roleRepository.findOne(TNUtils.COM_MEM_ROLE_ID);
			roles.add(role);
		} else
		{
			List<Long> roleIds = userDTO.getRoleIds();

			for (Long roleId : roleIds)
			{
				role = roleRepository.findOne(roleId);
				roles.add(role);
			}
		}

		user.setRoles(roles);
		
		User userAfterSave = userRepository.save(user);
		UserDTO userDTOAfterSave = new UserDTO(userAfterSave);
		return userDTOAfterSave;
	}

	@Override
	public List<UserDTO> getPIUsers(String roleName)
	{
		List<User> users = userRepository.findByStatusAndRolesRoleName(UserStatus.ACTIVE, roleName);
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (User user : users)
		{
			UserDTO userDTO = new UserDTO(user);
			userDTOs.add(userDTO);
		}

		return userDTOs;
	}

	@Override
	public List<UserDTO> getUsersByRoles(List<Long> roleIds)
	{
		List<User> users = userRepository.findByStatusAndRolesIdIn(roleIds);
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (User user : users)
		{
			UserDTO userDTO = new UserDTO(user);
			userDTOs.add(userDTO);
		}

		return userDTOs;
	}

}
