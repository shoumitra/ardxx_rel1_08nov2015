package com.tn.security.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 
 * ARDXXUtils class represents contants and utility methods
 * 
 * @author naikraj
 *
 */
public final class TNUtils
{

	private TNUtils()
	{
		super();
	}

	public static final String EMPTY_STRING = " ";
	public static final String SQL_UPDATE = "UPDATE";
	public static final String SQL_SET = "SET";
	public static final String SQL_SELECT = "SELECT";
	public static final String SQL_DELETE = "DELETE";
	public static final String SQL_WHERE = "WHERE";
	public static final String SQL_IN = "IN";
	public static final String SQL_FROM = "FROM";
	public static final String SQL_EQUAL = "=";
	public static final String SQL_SINGLE_QUATE = "'";
	public static final String SQL_SINGLE_QUATE_REPLACE = "''";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String MESSAGE = "message";
	public static final Long COM_MEM_ROLE_ID = 7L;

	/**
	 * isEmpty method check the value is null or empty.
	 * 
	 * @param value
	 * @return true if value is null or empty else false
	 */
	public static boolean isEmpty(String value)
	{
		if (value == null)
		{
			return true;
		} else
		{
			return value.trim().isEmpty();
		}
	}

	public static Date dateFormat(String value, String pattern)
	{
		try
		{
			if (value != null && !value.trim().isEmpty())
			{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				return simpleDateFormat.parse(value);
			} else
			{
				return null;
			}
		} catch (Exception e)
		{
			return null;
		}

	}

	/**
	 * generateAnimalId method will generate the animal id.
	 * 
	 * @return alphanumeric value
	 */
	public static String generateAnimalId()
	{
		String value = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(6);
		for (int i = 0; i < 6; i++)
			sb.append(value.charAt(rnd.nextInt(value.length())));
		return sb.toString();
	}

}
