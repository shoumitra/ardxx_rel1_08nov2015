package com.tn.security.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;

/**
 * 
 * @author Raj
 *
 */
public class ErrorPayload implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Map<String, String>> errorMessages = new ArrayList<Map<String, String>>();

	public ErrorPayload(String... errorCode)
	{
		String errorMessage = null;
		Map<String, String> error = new HashMap<String, String>();
		for (String code : errorCode)
		{

			errorMessage = Messages.properties.getProperty(code + "."+TNUtils.MESSAGE);
			error.put(code, errorMessage);

		}

		this.errorMessages.add(error);
	}

	public List<Map<String, String>> getErrorMessages()
	{
		return errorMessages;
	}

	public void setErrorMessages(List<Map<String, String>> errorMessages)
	{
		this.errorMessages = errorMessages;
	}

}
