package com.ardxx.ardxxapi.protocol.service;

import java.util.List;

import com.ardxx.ardxxapi.protocol.domain.AnesthesiaMethod;
import com.ardxx.ardxxapi.protocol.domain.AnimalRequirements;
import com.ardxx.ardxxapi.protocol.domain.BioAnimalMaterial;
import com.ardxx.ardxxapi.protocol.domain.Comment;
import com.ardxx.ardxxapi.protocol.domain.DescriptionOfExperiment;
import com.ardxx.ardxxapi.protocol.domain.EuthanasiaMethod;
import com.ardxx.ardxxapi.protocol.domain.HazardousAgents;
import com.ardxx.ardxxapi.protocol.domain.MajorSurvialSurgery;
import com.ardxx.ardxxapi.protocol.domain.NumberOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.PICertification;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.protocol.domain.ProtocolAdministrativeData;
import com.ardxx.ardxxapi.protocol.domain.RationalUseOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.RecordPainDistressCategory;
import com.ardxx.ardxxapi.protocol.domain.Review;
import com.ardxx.ardxxapi.protocol.domain.SpecialRequirementOfStudy;
import com.ardxx.ardxxapi.protocol.domain.StudyObjective;
import com.ardxx.ardxxapi.protocol.domain.Transportation;
import com.ardxx.ardxxapi.protocol.dto.ProtocolDTO;
import com.ardxx.ardxxapi.protocol.dto.UpdateJson;

/**
 * ProtocolService used to write business logic for Protocol entity.
 * 
 * @author naikraj
 *
 */
public interface ProtocolService
{

	/**
	 * Create protocol method creates the new protocol.
	 * 
	 * @param protocol
	 * @return protocol
	 */
	Protocol createProtocol(UpdateJson updateJson);

	/**
	 * Get protocol method returns the protocol information for perticular provided protocol id.
	 * 
	 * @param protocolId
	 * @return Returns updated {@link Protocol}
	 */
	Protocol getProtocol(Long protocolId);

	/**
	 * Method to return list of Protocols.
	 * 
	 * @return List<{@link ProtocolDTO}>
	 */
	List<ProtocolDTO> getProtocolList(int page, int size);

	/**
	 * Method to delete protocol for a given protocol id
	 * 
	 * @param protocolId
	 * @return deleted status
	 */
	String deleteProtocol(Long protocolId) throws Exception;

	/**
	 * Update protocol method updates the protocol information
	 * 
	 * @param {@link UpdateJson}
	 * @param protocolId
	 * @return
	 * @throws Exception
	 */
	Protocol updateProtocol(UpdateJson updateJson, Long protocolId) throws Exception;

	/**
	 * Method to update the Animal Requirements values
	 * 
	 * @param updateJson
	 * @param pid
	 * @param aid
	 * @return
	 */
	AnimalRequirements updateAnimalRequirements(UpdateJson updateJson, Long pid, Long aid) throws Exception;

	/**
	 * @param updateJson
	 * @param pid
	 * @param paid
	 * @param opid
	 * @return
	 * @throws Exception
	 */
	NumberOfAnimals updateNumberOfAnimals(UpdateJson updateJson, Long pid, Long paid, Long opid) throws Exception;

	/**
	 * Method to update the Protocol Administrative data values
	 * 
	 * @param updateJson
	 * @param pid
	 * @param paid
	 * @return
	 */
	ProtocolAdministrativeData updateProtocolAdministrativeData(UpdateJson updateJson, Long pid, Long paid) throws Exception;

	/**
	 * @param updateJson
	 * @param pid
	 * @param paid
	 * @param aapid
	 * @return
	 */
	ProtocolAdministrativeData updateAnimalProceduresPersonals(UpdateJson updateJson, Long pid, Long paid, Long aapid);

	/**
	 * @param updateJson
	 * @param pid
	 * @param paid
	 * @param coid
	 * @return
	 */
	ProtocolAdministrativeData updateCoInvestigators(UpdateJson updateJson, Long pid, Long paid, Long coid);

	/**
	 * @param updateJson
	 * @param pid
	 * @param paid
	 * @param opid
	 * @return
	 */
	ProtocolAdministrativeData updateOtherPersonals(UpdateJson updateJson, Long pid, Long paid, Long opid);

	/**
	 * Method to update Transportation
	 * 
	 * @param updateJson
	 * @param pid
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	Transportation updateTransportation(UpdateJson updateJson, Long pid, Long tid) throws Exception;

	/**
	 * Method to update Study of Objective values
	 * 
	 * 
	 * @param updateJson
	 * @param pid
	 * @param tid
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	StudyObjective updateStudyObjective(UpdateJson updateJson, Long pid, Long tid, Long uid) throws Exception;

	/**
	 * Method to update RationalForUseOfAnimals
	 * 
	 * @param updateJson
	 * @param pid
	 * @param sid
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	RationalUseOfAnimals updateRationalForUseOfAnimals(UpdateJson updateJson, Long pid, Long sid, Long uid) throws Exception;

	/**
	 * Method to update MajorSurvialSurgery
	 * 
	 * @param updateJson
	 * @param pid
	 * @param sid
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	MajorSurvialSurgery updateMajorSurvialSurgery(UpdateJson updateJson, Long pid, Long sid, Long uid) throws Exception;

	/**
	 * Method to update DescriptionOfExperiment
	 * 
	 * @param updateJson
	 * @param pid
	 * @param did
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	DescriptionOfExperiment updateDescriptionOfExperiment(UpdateJson updateJson, Long pid, Long did, Long uid) throws Exception;

	/**
	 * addComment method adds the comment
	 * 
	 * @param updateJson
	 * @param pid
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	Comment addComment(UpdateJson updateJson, Long pid, Long cid, Long userId) throws Exception;

	/**
	 * @param updateJson
	 * @param pid
	 * @param sid
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	SpecialRequirementOfStudy updateSpecialRequirementOfStudy(UpdateJson updateJson, Long pid, Long sid, Long uid) throws Exception;

	/**
	 * 
	 * @param updateJson
	 * @param protocolId
	 * @param anesthesiamethodId
	 * @param userinputId
	 * @return
	 * @throws Exception
	 */
	AnesthesiaMethod updateAnesthesiaMethod(UpdateJson updateJson, Long protocolId, Long anesthesiamethodId, Long userinputId) throws Exception;

	/**
	 * 
	 * @param updateJson
	 * @param protocolId
	 * @param recordPainOrDistressCategoryId
	 * @param usdacolumnbid
	 * @return
	 */
	RecordPainDistressCategory updateRecordPainOrDistressCategory(UpdateJson updateJson, Long protocolId, Long recordPainOrDistressCategoryId,
			Long usdacolumnbid) throws Exception;

	/**
	 * 
	 * @param updateJson
	 * @param protocolId
	 * @param recordPainOrDistressCategoryId
	 * @param userInputId
	 * @return
	 * @throws Exception
	 */
	RecordPainDistressCategory updateRecordPainOrDistressCategoryUserInput(UpdateJson updateJson, Long protocolId,
			Long recordPainOrDistressCategoryId, Long userInputId) throws Exception;

	/**
	 * 
	 * @param updateJson
	 * @param protocolId
	 * @param pICertificationId
	 * @return
	 * @throws Exception
	 */
	PICertification updatePICertification(UpdateJson updateJson, Long protocolId, Long pICertificationId) throws Exception;

	/**
	 * 
	 * @param updateJson
	 * @param protocolId
	 * @param pICertificationId
	 * @param userInputId
	 * @return
	 */
	PICertification updatePICertification(UpdateJson updateJson, Long protocolId, Long pICertificationId, Long userInputId);

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param bioAnimalMaterialId
	 * @param bioMaterialsUsedId
	 * @return throws Exception
	 */
	BioAnimalMaterial updateBioAnimalMaterialBiomaterialsUsed(UpdateJson updateJson, Long protocolId, Long bioAnimalMaterialId,
			Long bioMaterialsUsedId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param hazardousAgentsId
	 * @param userInputId
	 * @return throws Exception
	 */
	HazardousAgents updateHazardousAgents(UpdateJson updateJson, Long protocolId, Long hazardousAgentsId, Long userInputId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param bioAnimalMaterialId
	 * @return
	 * @throws Exception
	 */
	BioAnimalMaterial updateBioAnimalMaterial(UpdateJson updateJson, Long protocolId, Long bioAnimalMaterialId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param majorSurvialSurgeryId
	 * @return
	 * @throws Exception
	 */
	MajorSurvialSurgery updateMajorSurvialSurgery(UpdateJson updateJson, Long protocolId, Long majorSurvialSurgeryId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param anesthesiamethodId
	 * @return
	 * @throws Exception
	 */
	AnesthesiaMethod updateAnesthesiaMethod(UpdateJson updateJson, Long protocolId, Long anesthesiamethodId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param euthanasiamethodId
	 * @param userinputId
	 * @return
	 * @throws Exception
	 */
	EuthanasiaMethod updateEuthanasiaMethodUserInput(UpdateJson updateJson, Long protocolId, Long euthanasiamethodId, Long userinputId)
			throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param euthanasiamethodId
	 * @return
	 * @throws Exception
	 */
	EuthanasiaMethod updateEuthanasiaMethod(UpdateJson updateJson, Long protocolId, Long euthanasiamethodId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param hazardousAgentsId
	 * @return
	 * @throws Exception
	 */
	HazardousAgents updateHazardousAgents(UpdateJson updateJson, Long protocolId, Long hazardousAgentsId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param specialRequirementOfStudyId
	 * @return
	 * @throws Exception
	 */
	SpecialRequirementOfStudy updateSpecialRequirementOfStudy(UpdateJson updateJson, Long protocolId, Long specialRequirementOfStudyId)
			throws Exception;

	/**
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	List<ProtocolDTO> getProtocolList(Long userId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param reviewId
	 * @return
	 * @throws Exception
	 */
	Review updateReview(UpdateJson updateJson, Long protocolId, Long reviewId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param reviewId
	 * @return
	 * @throws Exception
	 */
	Review addReviewUser(UpdateJson updateJson, Long protocolId, Long reviewId) throws Exception;

	/**
	 * @param updateJson
	 * @param protocolId
	 * @param piuserId
	 * @return
	 * @throws Exception
	 */
	Protocol updatePIUser(UpdateJson updateJson, Long protocolId, Long piuserId) throws Exception;

	/**
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	List<ProtocolDTO> getProtocolListByPIUser(Long userId) throws Exception;

	/**
	 * @param protocolId
	 * @param animalRequirementsId
	 * @return
	 * @throws Exception
	 */
	AnimalRequirements deleteAnimalRequirements(Long protocolId, Long animalRequirementsId) throws Exception;

	/**
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param coInvestigatorsId
	 * @return
	 */
	ProtocolAdministrativeData deleteCoInvestigators(Long protocolId, Long protocolAdministrativeDataId, Long coInvestigatorsId);

	/**
	 * @param piuserId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	List<ProtocolDTO> getProtocolsByUserAndStatus(Long piuserId, String status) throws Exception;

	/**
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param animalProceduresPersonalsId
	 * @return
	 */
	ProtocolAdministrativeData deleteAnimalProceduresPersonals(Long protocolId, Long protocolAdministrativeDataId, Long animalProceduresPersonalsId);

	/**
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param otherPersonalsId
	 * @return
	 */
	ProtocolAdministrativeData deleteOtherPersonals(Long protocolId, Long protocolAdministrativeDataId, Long otherPersonalsId);

	public void deleteBioAnimalMaterialBiomaterialsUsed(Long protocolId, Long bioAnimalMaterialId, Long bioMaterialsUsedId) throws Exception;

}
