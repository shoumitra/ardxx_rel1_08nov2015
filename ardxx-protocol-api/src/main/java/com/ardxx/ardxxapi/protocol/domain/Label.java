package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Questions class represetnts the all questions from different section.
 * 
 * @author naikraj
 *
 */
@Entity
public class Label implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String labelKey;
	private String labelDescription;
	private String parentKey;

	@ManyToOne
	private Section section;

	public Section getSection()
	{
		return section;
	}

	public void setSection(Section section)
	{
		this.section = section;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getParentKey()
	{
		return parentKey;
	}

	public void setParentKey(String parentKey)
	{
		this.parentKey = parentKey;
	}

	public String getLabelKey()
	{
		return labelKey;
	}

	public void setLabelKey(String labelKey)
	{
		this.labelKey = labelKey;
	}

	public String getLabelDescription()
	{
		return labelDescription;
	}

	public void setLabelDescription(String labelDescription)
	{
		this.labelDescription = labelDescription;
	}

}
