package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity to hold biological products for use in animals. There could be one or more of this entity in BioAnimalMaterial
 * 
 * @author gopikrishnappa
 *
 */
@Entity
public class BioMaterialData implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String source;
	private boolean sterileAttenuated;
	private boolean useInRodent;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public boolean isSterileAttenuated()
	{
		return sterileAttenuated;
	}

	public void setSterileAttenuated(boolean sterileAttenuated)
	{
		this.sterileAttenuated = sterileAttenuated;
	}

	public boolean isUseInRodent()
	{
		return useInRodent;
	}

	public void setUseInRodent(boolean useInRodent)
	{
		this.useInRodent = useInRodent;
	}

}
