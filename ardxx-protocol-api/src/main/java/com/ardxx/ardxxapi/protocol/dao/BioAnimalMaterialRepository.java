package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.BioAnimalMaterial;

public interface BioAnimalMaterialRepository  extends PagingAndSortingRepository<BioAnimalMaterial, Long>
{

}
