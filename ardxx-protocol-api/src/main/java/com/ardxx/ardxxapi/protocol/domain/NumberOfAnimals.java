package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NumberOfAnimals implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String species;

	private int year1;

	private int year2;

	private int year3;

	private int total;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSpecies()
	{
		return species;
	}

	public void setSpecies(String species)
	{
		this.species = species;
	}

	public int getYear1()
	{
		return year1;
	}

	public void setYear1(int year1)
	{
		this.year1 = year1;
	}

	public int getYear2()
	{
		return year2;
	}

	public void setYear2(int year2)
	{
		this.year2 = year2;
	}

	public int getYear3()
	{
		return year3;
	}

	public void setYear3(int year3)
	{
		this.year3 = year3;
	}

	public int getTotal()
	{
		return total;
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

}
