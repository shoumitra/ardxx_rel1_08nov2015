package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class RecordPainDistressCategory implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private NumberOfAnimalsEachYear USDAColumnB;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private NumberOfAnimalsEachYear USDAColumnC;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private NumberOfAnimalsEachYear USDAColumnD;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private NumberOfAnimalsEachYear USDAColumnE;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private UserInput narativeOfProcedures;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public NumberOfAnimalsEachYear getUSDAColumnB()
	{
		return USDAColumnB;
	}

	public void setUSDAColumnB(NumberOfAnimalsEachYear uSDAColumnB)
	{
		USDAColumnB = uSDAColumnB;
	}

	public NumberOfAnimalsEachYear getUSDAColumnC()
	{
		return USDAColumnC;
	}

	public void setUSDAColumnC(NumberOfAnimalsEachYear uSDAColumnC)
	{
		USDAColumnC = uSDAColumnC;
	}

	public NumberOfAnimalsEachYear getUSDAColumnD()
	{
		return USDAColumnD;
	}

	public void setUSDAColumnD(NumberOfAnimalsEachYear uSDAColumnD)
	{
		USDAColumnD = uSDAColumnD;
	}

	public NumberOfAnimalsEachYear getUSDAColumnE()
	{
		return USDAColumnE;
	}

	public void setUSDAColumnE(NumberOfAnimalsEachYear uSDAColumnE)
	{
		USDAColumnE = uSDAColumnE;
	}

	public UserInput getNarativeOfProcedures()
	{
		return narativeOfProcedures;
	}

	public void setNarativeOfProcedures(UserInput narativeOfProcedures)
	{
		this.narativeOfProcedures = narativeOfProcedures;
	}

}
