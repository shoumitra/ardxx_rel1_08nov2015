package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.tn.security.basic.domain.User;

/**
 * Protocol class represents animal protocol
 * 
 * @author gopikrishnappa
 *
 */
@Entity
public class Protocol implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String title;
	private String proposalNumber;
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvalDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date annualReviewDueFirstYear;
	@Temporal(TemporalType.TIMESTAMP)
	private Date annualReviewDueSecondYear;
	private String transaction;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private ProtocolStatus status = ProtocolStatus.PENDING;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private ProtocolAdministrativeData protocolAdministrativeData = new ProtocolAdministrativeData();

	@ElementCollection
	private Set<AnimalRequirements> animalRequirements;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Transportation transportation;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private StudyObjective studyObjective = new StudyObjective();

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private RationalUseOfAnimals rationalUseOfAnimals = new RationalUseOfAnimals();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private DescriptionOfExperiment descriptionOfExperiment;// = new
															// DescriptionOfExperiment();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private MajorSurvialSurgery majorSurvialSurgery;// = new
													// MajorSurvialSurgery();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private RecordPainDistressCategory recordPainDistressCategory;// = new
																	// RecordPainDistressCategory();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private AnesthesiaMethod anesthesiaMethod;// = new AnesthesiaMethod();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private EuthanasiaMethod euthanasiaMethod;// = new EuthanasiaMethod();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private HazardousAgents hazardousAgents = new HazardousAgents();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private BioAnimalMaterial bioAnimalMaterial;// = new BioAnimalMaterial();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private SpecialRequirementOfStudy specialRequirementOfStudy; // = new
																	// SpecialRequirementOfStudy();

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private PICertification pICertification; // = new PICertification();

	@ElementCollection
	private List<Comment> comments;

	@ManyToOne
	private User createdBy;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private Review review;

	@OneToOne
	private User piUser;

	private Date createdDate;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getProposalNumber()
	{
		return proposalNumber;
	}

	public void setProposalNumber(String proposalNumber)
	{
		this.proposalNumber = proposalNumber;
	}

	public Date getApprovalDate()
	{
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate)
	{
		this.approvalDate = approvalDate;
	}

	public Date getExpirationDate()
	{
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	public ProtocolStatus getStatus()
	{
		return status;
	}

	public void setStatus(ProtocolStatus status)
	{
		this.status = status;
	}

	public ProtocolAdministrativeData getProtocolAdministrativeData()
	{
		return protocolAdministrativeData;
	}

	public void setProtocolAdministrativeData(ProtocolAdministrativeData protocolAdministrativeData)
	{
		this.protocolAdministrativeData = protocolAdministrativeData;
	}

	public Set<AnimalRequirements> getAnimalRequirements()
	{
		return animalRequirements;
	}

	public void setAnimalRequirements(Set<AnimalRequirements> animalRequirements)
	{
		this.animalRequirements = animalRequirements;
	}

	public Transportation getTransportation()
	{
		return transportation;
	}

	public void setTransportation(Transportation transportation)
	{
		this.transportation = transportation;
	}

	public String getTransaction()
	{
		return transaction;
	}

	public void setTransaction(String transaction)
	{
		this.transaction = transaction;
	}

	public StudyObjective getStudyObjective()
	{
		return studyObjective;
	}

	public void setStudyObjective(StudyObjective studyObjective)
	{
		this.studyObjective = studyObjective;
	}

	public RationalUseOfAnimals getRationalUseOfAnimals()
	{
		return rationalUseOfAnimals;
	}

	public void setRationalUseOfAnimals(RationalUseOfAnimals rationalUseOfAnimals)
	{
		this.rationalUseOfAnimals = rationalUseOfAnimals;
	}

	public DescriptionOfExperiment getDescriptionOfExperiment()
	{
		return descriptionOfExperiment;
	}

	public void setDescriptionOfExperiment(DescriptionOfExperiment descriptionOfExperiment)
	{
		this.descriptionOfExperiment = descriptionOfExperiment;
	}

	public MajorSurvialSurgery getMajorSurvialSurgery()
	{
		return majorSurvialSurgery;
	}

	public void setMajorSurvialSurgery(MajorSurvialSurgery majorSurvialSurgery)
	{
		this.majorSurvialSurgery = majorSurvialSurgery;
	}

	public RecordPainDistressCategory getRecordPainDistressCategory()
	{
		return recordPainDistressCategory;
	}

	public void setRecordPainDistressCategory(RecordPainDistressCategory recordPainDistressCategory)
	{
		this.recordPainDistressCategory = recordPainDistressCategory;
	}

	public AnesthesiaMethod getAnesthesiaMethod()
	{
		return anesthesiaMethod;
	}

	public void setAnesthesiaMethod(AnesthesiaMethod anesthesiaMethod)
	{
		this.anesthesiaMethod = anesthesiaMethod;
	}

	public EuthanasiaMethod getEuthanasiaMethod()
	{
		return euthanasiaMethod;
	}

	public void setEuthanasiaMethod(EuthanasiaMethod euthanasiaMethod)
	{
		this.euthanasiaMethod = euthanasiaMethod;
	}

	public HazardousAgents getHazardousAgents()
	{
		return hazardousAgents;
	}

	public void setHazardousAgents(HazardousAgents hazardousAgents)
	{
		this.hazardousAgents = hazardousAgents;
	}

	public BioAnimalMaterial getBioAnimalMaterial()
	{
		return bioAnimalMaterial;
	}

	public void setBioAnimalMaterial(BioAnimalMaterial bioAnimalMaterial)
	{
		this.bioAnimalMaterial = bioAnimalMaterial;
	}

	public SpecialRequirementOfStudy getSpecialRequirementOfStudy()
	{
		return specialRequirementOfStudy;
	}

	public void setSpecialRequirementOfStudy(SpecialRequirementOfStudy specialRequirementOfStudy)
	{
		this.specialRequirementOfStudy = specialRequirementOfStudy;
	}

	public PICertification getpICertification()
	{
		return pICertification;
	}

	public void setpICertification(PICertification pICertification)
	{
		this.pICertification = pICertification;
	}

	public List<Comment> getComments()
	{
		return comments;
	}

	public void setComments(List<Comment> comments)
	{
		this.comments = comments;
	}

	public Date getAnnualReviewDueFirstYear()
	{
		return annualReviewDueFirstYear;
	}

	public void setAnnualReviewDueFirstYear(Date annualReviewDueFirstYear)
	{
		this.annualReviewDueFirstYear = annualReviewDueFirstYear;
	}

	public Date getAnnualReviewDueSecondYear()
	{
		return annualReviewDueSecondYear;
	}

	public void setAnnualReviewDueSecondYear(Date annualReviewDueSecondYear)
	{
		this.annualReviewDueSecondYear = annualReviewDueSecondYear;
	}

	public User getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(User createdBy)
	{
		this.createdBy = createdBy;
	}

	public Review getReview()
	{
		return review;
	}

	public void setReview(Review review)
	{
		this.review = review;
	}

	public User getPiUser()
	{
		return piUser;
	}

	public void setPiUser(User piUser)
	{
		this.piUser = piUser;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

}
