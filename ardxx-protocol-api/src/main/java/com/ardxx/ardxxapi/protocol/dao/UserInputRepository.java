package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.UserInput;

public interface UserInputRepository extends PagingAndSortingRepository<UserInput, Long>
{

}
