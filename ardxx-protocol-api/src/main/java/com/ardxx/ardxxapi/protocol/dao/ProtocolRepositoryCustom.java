package com.ardxx.ardxxapi.protocol.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface ProtocolRepositoryCustom
{

	/**
	 * Method to update a specified field value for a given table
	 * 
	 * @param field
	 * @param value
	 * @param id
	 * @param table
	 */
	public void updateEntity(String field, Object value, long id, String table);

	/**
	 * getEntity method returns the selected table Object.
	 * 
	 * @param id
	 * @param table
	 * @return
	 */
	public Object getEntity(long id, String table);

	
}
