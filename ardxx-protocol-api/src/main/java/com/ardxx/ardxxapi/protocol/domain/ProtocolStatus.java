/**
 * 
 */
package com.ardxx.ardxxapi.protocol.domain;

/**
 * @author gopikrishnappa
 *
 */
public enum ProtocolStatus
{
	PENDING, APPROVED, ACTIVE, HOLD, CLOSED, CONDITIONAL_APPROVAL, EXPIRED, REVIEW, PRE_REVIEW
}
