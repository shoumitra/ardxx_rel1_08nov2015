package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.BioMaterialData;

public interface BioMaterialDataRepository extends PagingAndSortingRepository<BioMaterialData, Long>
{

}
