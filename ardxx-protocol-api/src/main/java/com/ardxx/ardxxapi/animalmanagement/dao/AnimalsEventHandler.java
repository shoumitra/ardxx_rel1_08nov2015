package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.service.AnimalHistoryServiceImpl;

@Configurable
@Component
public class AnimalsEventHandler
{

	@Autowired
	AnimalHistoryRepository animalHistoryRepository;

	@Autowired
	AnimalHistoryServiceImpl animalHistoryServiceImpl;

	@PersistenceContext
	private EntityManager entityManager;

	@PostUpdate
	public void handlePostUpdate(Animals animals)
	{
		saveAnimalHoistory(animals);
	}

	@PostPersist
	public void handleBeforeSave(Animals animals)
	{
		saveAnimalHoistory(animals);
	}

	private void saveAnimalHoistory(Animals animals)
	{
		WebApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext((ServletRequest) RequestContextHolder
				.currentRequestAttributes().resolveReference(RequestAttributes.REFERENCE_REQUEST));
		animalHistoryServiceImpl = applicationContext.getBean(AnimalHistoryServiceImpl.class);

		AnimalHistory animalHistory = new AnimalHistory();
		animalHistory.setAnimalTableId(animals.getId());
		animalHistory.setAnimalId(animals.getAnimalId());
		animalHistory.setClient(animals.getClient());
		animalHistory.setDateOfBirth(animals.getDateOfBirth());
		animalHistory.setDepartedDate(animals.getDepartedDate());
		animalHistory.setOldId(animals.getOldId());
		animalHistory.setOrigin(animals.getOrigin());
		animalHistory.setRecievedDate(animals.getRecievedDate());
		animalHistory.setRoomNum(animals.getRoomNum());
		animalHistory.setSex(animals.getSex());
		animalHistory.setSource(animals.getSource());
		animalHistory.setSpecies(animals.getSpecies());
		animalHistory.setStatus(animals.getStatus());
		animalHistory.setWeight(animals.getWeight());
		animalHistory.setStrain(animals.getStrain());
		animalHistory.setOtherStrain(animals.getOtherStrain());
		animalHistory.setOtherSpecies(animals.getOtherSpecies());
		animalHistory.setComments(animals.getComments());
		animalHistory.setAddedDate(new Date());
		animalHistory.setChargeCode(animals.getChargeCode());
		
		if(animals.getVetTech() != null)
		{
			animalHistory.setVetTech(animals.getVetTech().getId());
		}
		
		animalHistory.setSedationRequired(animals.isSedationRequired());
		animalHistory.setSubstance(animals.getSubstance());
		animalHistory.setDosage(animals.getDosage());
		animalHistory.setTemperature(animals.getTemperature());
		animalHistory.setPulse(animals.getPulse());
		animalHistory.setRespiration(animals.getRespiration());
		animalHistory.setNotes(animals.getNotes());
		animalHistory.setWeight(animals.getWeight());
		animalHistory.setStudyId(animals.getStudyId());
		animalHistory.setProcedureId(animals.getProcedureId());
		animalHistory.setScheduleId(animals.getScheduleId());
		animalHistory.setVolume(animals.getVolume());
		animalHistory.setBuilding(animals.getBuilding());
		
		animalHistoryServiceImpl.saveAnimalHistory(animalHistory);
	}

}
