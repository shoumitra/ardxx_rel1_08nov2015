package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ImportFileHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false)
	private Long id;
	
	private Date uploadedDate;

	private String uploadedBy;

	private String format;

	private String status;
	
	@Column(length=65000)
	private String header;

	@ElementCollection
	private List<ImportFileHistoryDetails> importFileDetails;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Date getUploadedDate()
	{
		return uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate)
	{
		this.uploadedDate = uploadedDate;
	}

	public String getUploadedBy()
	{
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy)
	{
		this.uploadedBy = uploadedBy;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{
		this.format = format;
	}

	public List<ImportFileHistoryDetails> getImportFileDetails()
	{
		return importFileDetails;
	}

	public void setImportFileDetails(List<ImportFileHistoryDetails> importFileDetails)
	{
		this.importFileDetails = importFileDetails;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

}
