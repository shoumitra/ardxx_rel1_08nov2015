package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTestDetails;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalsTPRWDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleAnimalDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleCloneDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StorageLabImplDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyProcedureDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.VolumeDTO;
import com.ardxx.ardxxapi.common.domain.Constants;

public interface ProcedureService
{

	StudyProcedure createProcedure(Long studyId,StudyProcedure procedure) throws Exception;

	StudyProcedure getProcedure(Long procedureId)throws Exception;

	Page<StudyProcedure> getProcedureList(int page, int size)throws Exception;

	StudyProcedure assignTechnician(Long studyId, Long procedureId, String type, Long userId)throws Exception;

	List<Constants> getAllProcedureValues()throws Exception;

	StudyProcedure assignAnimalsToProcedure(Long studyId, Long procedureId, StudyDTO studyDTO)throws Exception;

	StudyProcedure deleteAnimalsFromProcedure(Long studyId, Long procedureId, Long animalId)throws Exception;

	StudyProcedure scheduleProcedure(Long studyId, Long procedureId, ProcedureSchedule procedureSchedule) throws Exception;

	StudyProcedure createProcedureTests(Long studyId, Long procedureId, ProcedureTest procedureTest) throws Exception;

	StudyProcedure editProcedure(Long studyId, Long procedureId, StudyProcedure procedure) throws Exception;

	StudyProcedure changeProcedureStatus(Long studyId, Long procedureId, String status)throws Exception;

	ProcedureSchedule updateScheduleProcedure(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule) throws Exception;

	void deleteScheduleProcedure(Long studyId, Long procedureId, Long scheduleId) throws Exception;

	Iterable<StudyProcedureDTO> getProcedureSchedules(Long userid)throws Exception;

	ProcedureSchedule updateAnimalsInSchedule(Long studyId, Long procedureId, Long scheduleId, Long animalId,ScheduleAnimalDTO scheduleAnimalDTO);

	StudyProcedure getProcedures(Long scheduleId) throws Exception;

	Iterable<StudyProcedureDTO> getProcedureSchedulesForLabTec(Long userid) throws Exception;

	List<AnimalHistory> getAnimalListForSchedule(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule) throws Exception;
	
	List<Animals> findAnimalsByProcedureId(Long procedureId) throws Exception;

	Iterable<StudyProcedureDTO> getProcedureSchedulesForPM() throws Exception;

	Iterable<StudyProcedureDTO> getProcedureSchedulesForStudy(Long studyId) throws Exception;

	Iterable<StudyProcedureDTO> cloneProcedureSchedules(Long studyId, ScheduleCloneDTO scheduleCloneDTO) throws Exception;

	Iterable<Animals> calculateVolume(Long studyId, Long scheduleid, VolumeDTO volumeDTO) throws Exception;

	Iterable<Animals> updateTPRWvalue(Long studyId, Long procedureId, Long scheduleId, List<AnimalsTPRWDTO> animalsTPRWDTO) throws Exception;

	Page<StudyProcedure> getClinicalProcedures(int page, int size) throws Exception;

	ProcedureTest editProcedureTests(Long studyId, Long procedureId, Long testId, ProcedureTest procedureTest)throws Exception;

	ProcedureTest storageLabImpl(Long studyId, Long procedureId, Long scheduleId, StorageLabImplDTO storageLabImplDTO) throws Exception;

	public ProcedureTestDetails getStorageLabDetails(Long studyId, Long procedureId, Long scheduleId, Long testId) throws Exception;
	
	public ProcedureTestDetails getStorageLabDetailsById(Long studyId, Long procedureId, Long scheduleId, Long storagelabimplId) throws Exception;

}
