package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProcedureTestDetails implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private Long testId;
	private String building;
	private int freezer;
	private int shelf;
	private int rack;
	private int position;
	private Long scheduleId;
	private Long procedureId;
	private Long studyId;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Long getTestId()
	{
		return testId;
	}

	public void setTestId(Long testId)
	{
		this.testId = testId;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	public int getFreezer()
	{
		return freezer;
	}

	public void setFreezer(int freezer)
	{
		this.freezer = freezer;
	}

	public int getShelf()
	{
		return shelf;
	}

	public void setShelf(int shelf)
	{
		this.shelf = shelf;
	}

	public int getRack()
	{
		return rack;
	}

	public void setRack(int rack)
	{
		this.rack = rack;
	}

	public int getPosition()
	{
		return position;
	}

	public void setPosition(int position)
	{
		this.position = position;
	}

	public Long getScheduleId()
	{
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId)
	{
		this.scheduleId = scheduleId;
	}

	public Long getProcedureId()
	{
		return procedureId;
	}

	public void setProcedureId(Long procedureId)
	{
		this.procedureId = procedureId;
	}

	public Long getStudyId()
	{
		return studyId;
	}

	public void setStudyId(Long studyId)
	{
		this.studyId = studyId;
	}

}
