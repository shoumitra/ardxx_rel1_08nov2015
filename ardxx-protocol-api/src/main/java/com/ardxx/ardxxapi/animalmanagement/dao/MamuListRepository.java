package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.animalmanagement.domain.MamuList;

/**
 * 
 * @author Raj
 *
 */
public interface MamuListRepository extends PagingAndSortingRepository<MamuList, Long>
{

}
