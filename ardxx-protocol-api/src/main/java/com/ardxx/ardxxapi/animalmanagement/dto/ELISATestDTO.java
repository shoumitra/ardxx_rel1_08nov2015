package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;

public class ELISATestDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private Date collectedDate;
	private String result;
	private String value;
	private Long virus;
	private Long animals;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}
	
	public Date getCollectedDate()
	{
		return collectedDate;
	}

	public void setCollectedDate(Date collectedDate)
	{
		this.collectedDate = collectedDate;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(String result)
	{
		this.result = result;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Long getVirus()
	{
		return virus;
	}

	public void setVirus(Long virus)
	{
		this.virus = virus;
	}

	public Long getAnimals()
	{
		return animals;
	}

	public void setAnimals(Long animals)
	{
		this.animals = animals;
	}

}
