package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Naik
 *
 */
public class AnimalRequestUploadDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> animalsList = new ArrayList<String>();
	private String format;
	private String uploadedBy;
	private String antrimAccountNumber;
	private String studyId;
	private String studyDirector;
	private String fileHeader;

	public List<String> getAnimalsList()
	{
		return animalsList;
	}

	public void setAnimalsList(List<String> animalsList)
	{
		this.animalsList = animalsList;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{
		this.format = format;
	}

	public String getUploadedBy()
	{
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy)
	{
		this.uploadedBy = uploadedBy;
	}

	public String getAntrimAccountNumber()
	{
		return antrimAccountNumber;
	}

	public void setAntrimAccountNumber(String antrimAccountNumber)
	{
		this.antrimAccountNumber = antrimAccountNumber;
	}

	public String getStudyId()
	{
		return studyId;
	}

	public void setStudyId(String studyId)
	{
		this.studyId = studyId;
	}

	public String getStudyDirector()
	{
		return studyDirector;
	}

	public void setStudyDirector(String studyDirector)
	{
		this.studyDirector = studyDirector;
	}

	public String getFileHeader()
	{
		return fileHeader;
	}

	public void setFileHeader(String fileHeader)
	{
		this.fileHeader = fileHeader;
	}

}
