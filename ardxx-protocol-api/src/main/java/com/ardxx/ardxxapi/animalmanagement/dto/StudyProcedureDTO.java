package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;

public class StudyProcedureDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String studyName;

	private Long procedureId;
	private Date scheduleDate;
	private String status;
	private Long scheduleId;
	private String procedureType;

	private String tubeType;
	private String quantity;
	private String multipleTubes;
	private String unit;
	private String notes;
	
	private String material;
	private String materialPrep;
	private String deliveryDevice;
	private  String route;
	private String site;
	private String sitePrep;
	private float dosage;
	private String dosageUnit;
	private float volume;
	private String volumeUnit;

	
	private String genericName;
	private float concentration;
	private String concentrationUnit;
	private String frequency;
	
	
	public String getStudyName()
	{
		return studyName;
	}

	public void setStudyName(String studyName)
	{
		this.studyName = studyName;
	}

	public Long getProcedureId()
	{
		return procedureId;
	}

	public void setProcedureId(Long procedureId)
	{
		this.procedureId = procedureId;
	}

	public Date getScheduleDate()
	{
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate)
	{
		this.scheduleDate = scheduleDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Long getScheduleId()
	{
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId)
	{
		this.scheduleId = scheduleId;
	}

	public String getTubeType()
	{
		return tubeType;
	}

	public void setTubeType(String tubeType)
	{
		this.tubeType = tubeType;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getMultipleTubes()
	{
		return multipleTubes;
	}

	public void setMultipleTubes(String multipleTubes)
	{
		this.multipleTubes = multipleTubes;
	}

	public String getUnit()
	{
		return unit;
	}

	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getMaterial()
	{
		return material;
	}

	public void setMaterial(String material)
	{
		this.material = material;
	}

	public String getMaterialPrep()
	{
		return materialPrep;
	}

	public void setMaterialPrep(String materialPrep)
	{
		this.materialPrep = materialPrep;
	}

	public String getDeliveryDevice()
	{
		return deliveryDevice;
	}

	public void setDeliveryDevice(String deliveryDevice)
	{
		this.deliveryDevice = deliveryDevice;
	}

	public String getRoute()
	{
		return route;
	}

	public void setRoute(String route)
	{
		this.route = route;
	}

	public String getSite()
	{
		return site;
	}

	public void setSite(String site)
	{
		this.site = site;
	}

	public String getSitePrep()
	{
		return sitePrep;
	}

	public void setSitePrep(String sitePrep)
	{
		this.sitePrep = sitePrep;
	}

	public float getDosage()
	{
		return dosage;
	}

	public void setDosage(float dosage)
	{
		this.dosage = dosage;
	}

	public String getDosageUnit()
	{
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit)
	{
		this.dosageUnit = dosageUnit;
	}

	public float getVolume()
	{
		return volume;
	}

	public void setVolume(float volume)
	{
		this.volume = volume;
	}

	public String getVolumeUnit()
	{
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit)
	{
		this.volumeUnit = volumeUnit;
	}

	public String getGenericName()
	{
		return genericName;
	}

	public void setGenericName(String genericName)
	{
		this.genericName = genericName;
	}

	public float getConcentration()
	{
		return concentration;
	}

	public void setConcentration(float concentration)
	{
		this.concentration = concentration;
	}

	public String getConcentrationUnit()
	{
		return concentrationUnit;
	}

	public void setConcentrationUnit(String concentrationUnit)
	{
		this.concentrationUnit = concentrationUnit;
	}

	public String getFrequency()
	{
		return frequency;
	}

	public void setFrequency(String frequency)
	{
		this.frequency = frequency;
	}

	public String getProcedureType()
	{
		return procedureType;
	}

	public void setProcedureType(String procedureType)
	{
		this.procedureType = procedureType;
	}

}
