package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;

public interface ProcedureRepository extends PagingAndSortingRepository<StudyProcedure, Long>
{
	@Query("from StudyProcedure where vet_technician_id=:id ")
//	@Query(nativeQuery=true,value="from Study where id in (select study_id from study_procedures where procedures_id in (select id from study_procedure where vet_technician_id =:id))")
	public Iterable<StudyProcedure> findByVetTechnicianId(@Param("id") Long technicianId);
	
	@Query("from StudyProcedure where lab_technician_id=:id ")
//	@Query(nativeQuery=true,value="from Study where id in (select study_id from study_procedures where procedures_id in (select id from study_procedure where vet_technician_id =:id))")
	public Iterable<StudyProcedure> findByLabTechnicianId(@Param("id") Long technicianId);
	
	public Iterable<StudyProcedure> findByStatusNot(@Param("status") String status);

	StudyProcedure findByProcedureSchedulesId(@Param("id") Long id);
	
	@Query(nativeQuery=true,value="select multiple_tubes,notes,quantity,tube_type,unit from blood_sample where id = (select blood_sample_id from study_procedure where id = :id)")
	List<Object[]> findByBloodSample(@Param("id") Long id);
	
	@Query(nativeQuery=true,value="select material,material_prep,delivery_device,route,site,site_prep,dosage,dosage_unit,volume,volume_unit,notes from inoculation where id = (select inoculation_id from study_procedure where id = :id)")
	List<Object[]> findByInoculation(@Param("id") Long id);

	@Query(nativeQuery=true,value="select generic_name, concentration, concentration_unit, dosage, dosage_unit,route, frequency, notes from treatment where id = (select treatment_id from study_procedure where id = :id)")
	List<Object[]> findByTreatement(@Param("id") Long id);
	
	@Query(nativeQuery=true,value="select id from study_procedure where id = (select study_procedure_id from study_procedure_procedure_schedules where procedure_schedules_id = :id)")
	Long findById(@Param("id") Long id);
	
	@Query("select animals from StudyProcedure AS sp where sp.id=:id")
	List<Animals> findAnimalsByProcedureSchedulesId(@Param("id") Long id);
	
	Page<StudyProcedure> findByProcedureType(Pageable pageable,@Param("procedureType") String procedureType);
}
