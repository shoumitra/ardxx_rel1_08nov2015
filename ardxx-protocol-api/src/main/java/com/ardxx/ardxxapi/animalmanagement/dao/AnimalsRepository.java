package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.Animals;

/**
 * 
 * @author Raj
 *
 */
@Repository
public interface AnimalsRepository extends PagingAndSortingRepository<Animals, Long>, AnimalsRepositoryCustom
{
	public Animals findByAnimalId(@Param("animalId") String animalId);
	
	public List<Animals> findByStatus(@Param("status") String status);
}
