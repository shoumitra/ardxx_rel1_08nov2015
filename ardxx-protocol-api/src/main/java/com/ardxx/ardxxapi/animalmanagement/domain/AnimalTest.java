package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author Raj
 *
 */
@Entity
public class AnimalTest implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String antrimAccountNumber;
	private String studyId;
	private String studyDirector;
	private String fileHeader;

	@ElementCollection
	private List<AnimalTestResults> animalTestResults;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getAntrimAccountNumber()
	{
		return antrimAccountNumber;
	}

	public void setAntrimAccountNumber(String antrimAccountNumber)
	{
		this.antrimAccountNumber = antrimAccountNumber;
	}

	public String getStudyId()
	{
		return studyId;
	}

	public void setStudyId(String studyId)
	{
		this.studyId = studyId;
	}

	public String getStudyDirector()
	{
		return studyDirector;
	}

	public void setStudyDirector(String studyDirector)
	{
		this.studyDirector = studyDirector;
	}

	public String getFileHeader()
	{
		return fileHeader;
	}

	public void setFileHeader(String fileHeader)
	{
		this.fileHeader = fileHeader;
	}

	public List<AnimalTestResults> getAnimalTestResults()
	{
		return animalTestResults;
	}

	public void setAnimalTestResults(List<AnimalTestResults> animalTestResults)
	{
		this.animalTestResults = animalTestResults;
	}

}
