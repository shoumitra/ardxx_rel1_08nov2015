package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalTestResultDTO;

@Repository
public interface AnimalTestRepositoryCustom
{
	
	public List<AnimalTestResults> getCBCTestResults(AnimalTestResultDTO animalTestResultDTO);
}
