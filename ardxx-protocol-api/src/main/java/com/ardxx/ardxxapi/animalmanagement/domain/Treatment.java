package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Treatment implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String genericName;
	private float concentration;
	private String concentrationUnit;
	private float dosage;
	private String dosageUnit;
	private String route;
	private String frequency;
	private String notes;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getGenericName()
	{
		return genericName;
	}

	public void setGenericName(String genericName)
	{
		this.genericName = genericName;
	}

	public float getConcentration()
	{
		return concentration;
	}

	public void setConcentration(float concentration)
	{
		this.concentration = concentration;
	}

	public float getDosage()
	{
		return dosage;
	}

	public void setDosage(float dosage)
	{
		this.dosage = dosage;
	}

	public String getRoute()
	{
		return route;
	}

	public void setRoute(String route)
	{
		this.route = route;
	}

	public String getFrequency()
	{
		return frequency;
	}

	public void setFrequency(String frequency)
	{
		this.frequency = frequency;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getConcentrationUnit()
	{
		return concentrationUnit;
	}

	public void setConcentrationUnit(String concentrationUnit)
	{
		this.concentrationUnit = concentrationUnit;
	}

	public String getDosageUnit()
	{
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit)
	{
		this.dosageUnit = dosageUnit;
	}

}
