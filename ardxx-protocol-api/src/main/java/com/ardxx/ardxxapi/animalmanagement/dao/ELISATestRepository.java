package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.ELISATest;

/**
 * 
 * @author Raj
 *
 */
@Repository
public interface ELISATestRepository extends PagingAndSortingRepository<ELISATest, Long>
{
	@Query("from ELISATest AS elisa where elisa.animals.animalId=:animalId")
	Iterable<ELISATest> getElisaTestByAnimalId(@Param("animalId") String animalId);

}
