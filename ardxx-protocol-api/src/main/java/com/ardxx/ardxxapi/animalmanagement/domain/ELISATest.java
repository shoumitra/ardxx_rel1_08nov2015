package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Raj
 *
 */
@Entity
public class ELISATest implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private Date collectedDate;
	private String result;
	private String value;

	@ManyToOne
	private Virus virus;

	@ManyToOne
	private Animals animals;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Date getCollectedDate()
	{
		return collectedDate;
	}

	public void setCollectedDate(Date collectedDate)
	{
		this.collectedDate = collectedDate;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(String result)
	{
		this.result = result;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Virus getVirus()
	{
		return virus;
	}

	public void setVirus(Virus virus)
	{
		this.virus = virus;
	}

	public Animals getAnimals()
	{
		return animals;
	}

	public void setAnimals(Animals animals)
	{
		this.animals = animals;
	}

}