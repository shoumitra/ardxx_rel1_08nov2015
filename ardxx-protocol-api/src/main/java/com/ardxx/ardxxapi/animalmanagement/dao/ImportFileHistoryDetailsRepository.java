package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistoryDetails;

@Repository
public interface ImportFileHistoryDetailsRepository extends PagingAndSortingRepository<ImportFileHistoryDetails, Long>
{

}
