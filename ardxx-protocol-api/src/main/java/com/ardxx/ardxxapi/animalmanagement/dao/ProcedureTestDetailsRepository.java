package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTestDetails;

public interface ProcedureTestDetailsRepository extends PagingAndSortingRepository<ProcedureTestDetails, Long>
{
	ProcedureTestDetails findByTestId(Long testId);
}
