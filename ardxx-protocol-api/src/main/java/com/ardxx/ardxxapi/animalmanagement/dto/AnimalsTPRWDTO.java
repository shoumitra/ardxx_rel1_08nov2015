package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;

public class AnimalsTPRWDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String temperature;
	private String pulse;
	private String respiration;
	private String weight;
	private Long scheduleId;
	private Long procedureId;
	private Long studyId;
	private float volume;

	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getTemperature()
	{
		return temperature;
	}
	public void setTemperature(String temperature)
	{
		this.temperature = temperature;
	}
	public String getPulse()
	{
		return pulse;
	}
	public void setPulse(String pulse)
	{
		this.pulse = pulse;
	}
	public String getRespiration()
	{
		return respiration;
	}
	public void setRespiration(String respiration)
	{
		this.respiration = respiration;
	}
	public String getWeight()
	{
		return weight;
	}
	public void setWeight(String weight)
	{
		this.weight = weight;
	}
	public Long getScheduleId()
	{
		return scheduleId;
	}
	public void setScheduleId(Long scheduleId)
	{
		this.scheduleId = scheduleId;
	}
	public Long getProcedureId()
	{
		return procedureId;
	}
	public void setProcedureId(Long procedureId)
	{
		this.procedureId = procedureId;
	}
	public Long getStudyId()
	{
		return studyId;
	}
	public void setStudyId(Long studyId)
	{
		this.studyId = studyId;
	}
	public float getVolume()
	{
		return volume;
	}
	public void setVolume(float volume)
	{
		this.volume = volume;
	}
	
	
}
