package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SpeciesRangeDetail implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String columnHeader;
	@Column(nullable = true)
	private double mHigh;
	@Column(nullable = true)
	private double mLow;
	@Column(nullable = true)
	private double fHigh;
	@Column(nullable = true)
	private double fLow;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getColumnHeader()
	{
		return columnHeader;
	}

	public void setColumnHeader(String columnHeader)
	{
		this.columnHeader = columnHeader;
	}

	public double getmHigh()
	{
		return mHigh;
	}

	public void setmHigh(double mHigh)
	{
		this.mHigh = mHigh;
	}

	public double getmLow()
	{
		return mLow;
	}

	public void setmLow(double mLow)
	{
		this.mLow = mLow;
	}

	public double getfHigh()
	{
		return fHigh;
	}

	public void setfHigh(double fHigh)
	{
		this.fHigh = fHigh;
	}

	public double getfLow()
	{
		return fLow;
	}

	public void setfLow(double fLow)
	{
		this.fLow = fLow;
	}

}
