package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProcedureSchedule implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private Date scheduleDate;

	private String status;
	
	private Date completionDate;
	
	private Date actualCompletionDate;
	
	@ElementCollection
	private List<ProcedureTestDetails> procedureTestDetails;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Date getScheduleDate()
	{
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate)
	{
		this.scheduleDate = scheduleDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getCompletionDate()
	{
		return completionDate;
	}

	public void setCompletionDate(Date completionDate)
	{
		this.completionDate = completionDate;
	}

	public Date getActualCompletionDate()
	{
		return actualCompletionDate;
	}

	public void setActualCompletionDate(Date actualCompletionDate)
	{
		this.actualCompletionDate = actualCompletionDate;
	}

	public List<ProcedureTestDetails> getProcedureTestDetails()
	{
		return procedureTestDetails;
	}

	public void setProcedureTestDetails(List<ProcedureTestDetails> procedureTestDetails)
	{
		this.procedureTestDetails = procedureTestDetails;
	}


}
