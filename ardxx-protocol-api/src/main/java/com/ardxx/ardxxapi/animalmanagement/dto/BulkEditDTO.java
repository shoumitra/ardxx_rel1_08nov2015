package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;

public class BulkEditDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String client;
	private String chargeCode;
	private Long vetTech;
	private String building;
	private String roomNumber;
	private Long[] animalIds;
	private Date updateDate;

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getClient()
	{
		return client;
	}

	public void setClient(String client)
	{
		this.client = client;
	}

	public String getChargeCode()
	{
		return chargeCode;
	}

	public void setChargeCode(String chargeCode)
	{
		this.chargeCode = chargeCode;
	}

	public Long getVetTech()
	{
		return vetTech;
	}

	public void setVetTech(Long vetTech)
	{
		this.vetTech = vetTech;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	public String getRoomNumber()
	{
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber)
	{
		this.roomNumber = roomNumber;
	}

	public Long[] getAnimalIds()
	{
		return animalIds;
	}

	public void setAnimalIds(Long[] animalIds)
	{
		this.animalIds = animalIds;
	}

}
