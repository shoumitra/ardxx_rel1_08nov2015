package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;

public class ScheduleAnimalDTO implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean sedationRequired;
	private String substance;
	private String dosage;
	private String temperature;
	private String pulse;
	private String respiration;
	private String notes;
	private String weight;
	

	public boolean isSedationRequired()
	{
		return sedationRequired;
	}

	public void setSedationRequired(boolean sedationRequired)
	{
		this.sedationRequired = sedationRequired;
	}

	public String getSubstance()
	{
		return substance;
	}

	public void setSubstance(String substance)
	{
		this.substance = substance;
	}

	public String getDosage()
	{
		return dosage;
	}

	public void setDosage(String dosage)
	{
		this.dosage = dosage;
	}

	public String getTemperature()
	{
		return temperature;
	}

	public void setTemperature(String temperature)
	{
		this.temperature = temperature;
	}

	public String getPulse()
	{
		return pulse;
	}

	public void setPulse(String pulse)
	{
		this.pulse = pulse;
	}

	public String getRespiration()
	{
		return respiration;
	}

	public void setRespiration(String respiration)
	{
		this.respiration = respiration;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(String weight)
	{
		this.weight = weight;
	}

}
