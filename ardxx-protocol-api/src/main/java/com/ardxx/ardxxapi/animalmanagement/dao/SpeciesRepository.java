package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.Species;

@Repository
public interface SpeciesRepository extends PagingAndSortingRepository<Species, String>
{
	public Iterable<Species> findAll();
}
