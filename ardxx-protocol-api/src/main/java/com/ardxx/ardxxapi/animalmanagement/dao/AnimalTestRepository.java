package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTest;

@Repository
public interface AnimalTestRepository extends PagingAndSortingRepository<AnimalTest, Long>, AnimalTestRepositoryCustom
{

}
