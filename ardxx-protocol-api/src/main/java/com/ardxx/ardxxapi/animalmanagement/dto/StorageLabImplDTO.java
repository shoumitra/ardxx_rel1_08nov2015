package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;

public class StorageLabImplDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long testId;
	private String building;
	private int freezer;
	private int shelf;
	private int rack;
	private int position;

	public Long getTestId()
	{
		return testId;
	}

	public void setTestId(Long testId)
	{
		this.testId = testId;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	public int getFreezer()
	{
		return freezer;
	}

	public void setFreezer(int freezer)
	{
		this.freezer = freezer;
	}

	public int getShelf()
	{
		return shelf;
	}

	public void setShelf(int shelf)
	{
		this.shelf = shelf;
	}

	public int getRack()
	{
		return rack;
	}

	public void setRack(int rack)
	{
		this.rack = rack;
	}

	public int getPosition()
	{
		return position;
	}

	public void setPosition(int position)
	{
		this.position = position;
	}

}
