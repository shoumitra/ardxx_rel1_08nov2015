package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.ardxx.ardxxapi.common.ARDXXUtils;
import com.tn.security.basic.domain.CurrentUser;
import com.tn.security.basic.domain.Role;

@Service
public class AnimalManagementUserServiceImpl implements CurrentUserService
{

	@Override
	public boolean canEditProcedureDetails(CurrentUser currentUser, ProcedureTest procedureTest)
	{
		List<Role> roles = currentUser.getRole();
		boolean roleFound = false;
		String roleName = null;
		if (procedureTest.getDestination() != null && !procedureTest.getDestination().isEmpty())
		{
			if (!ARDXXUtils.isEmpty(procedureTest.getStorageRequirements()) 
					|| procedureTest.getTimeShipppedBy() != null || !ARDXXUtils.isEmpty(procedureTest.getShippedTo()))
			{
				if (procedureTest.getDestination().equalsIgnoreCase("STORAGE") || procedureTest.getDestination().equalsIgnoreCase("LABORATORY"))
				{
					roleName = "LAB_TEC";
				}

				if (procedureTest.getDestination().equalsIgnoreCase("CLIENT") || procedureTest.getDestination().equalsIgnoreCase("IDEXX"))
				{
					roleName = "VET_TEC";
				}

				for (Role role : roles)
				{
					if (role.getRoleName().equalsIgnoreCase(roleName))
					{
						roleFound = true;
					}
				}

				return roleFound;
			}
			
		}

		return true;
	}

}
