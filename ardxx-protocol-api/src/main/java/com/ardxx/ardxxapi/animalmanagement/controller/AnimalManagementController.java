package com.ardxx.ardxxapi.animalmanagement.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ardxx.ardxxapi.animalmanagement.controller.validator.AnimalsValidator;
import com.ardxx.ardxxapi.animalmanagement.controller.validator.Headers;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTest;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ELISATest;
import com.ardxx.ardxxapi.animalmanagement.domain.Format;
import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuList;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuTest;
import com.ardxx.ardxxapi.animalmanagement.domain.SpeciesDetail;
import com.ardxx.ardxxapi.animalmanagement.domain.Virus;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalRequestUploadDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalTestResultDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.BulkEditDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ELISATestDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.MamuTestDTO;
import com.ardxx.ardxxapi.animalmanagement.service.AnimalManagementService;
import com.ardxx.ardxxapi.common.ConstantsMap;
import com.ardxx.ardxxapi.common.Messages;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.exception.ARDXXErrorPayload;

/**
 * Rest controller to manage Animal details.
 * 
 * @author Raj
 *
 */
@RestController
@RequestMapping(value = "/animals")
public class AnimalManagementController
{

	@Autowired
	AnimalManagementService animalManagementService;
	
	@Autowired 
	ConstantsMap constantsMap;
	
	/**
	 * Ping method used to know the service is up or not.
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/ping/{name}", method = RequestMethod.GET)
	public String ping(@PathVariable("name") String name)
	{
		return "Hi ".concat(name).concat(" animal management service up and running!!!");
	}

	/**
	 * uploadAnimalDetails method upload the animal details.
	 * 
	 * @param animalsList
	 * @return
	 */
	@RequestMapping(value = "/uploadanimals", method = RequestMethod.POST)
	public String[] uploadAnimalDetails(@RequestBody AnimalRequestUploadDTO animalRequestUpload)
	{

		AnimalsValidator animalsValidator = new AnimalsValidator();
		Headers headers = null;
		String header = animalRequestUpload.getAnimalsList().get(0);
		if (Format.ANIMAL.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			headers = new Headers(animalRequestUpload.getAnimalsList().get(0));
			header = animalRequestUpload.getAnimalsList().get(0);

		} else if (Format.CBC.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			headers = new Headers(animalRequestUpload.getAnimalsList().get(4));
			header = animalRequestUpload.getAnimalsList().get(4);
		}
		
//		Iterable<Species> species =  animalManagementService.getAllSpecies();
		 
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> species = map.get("animals.species.type");
		
		List<String[]> responseList = animalsValidator.validateAnimals(animalRequestUpload,headers,species);
		String[] result = animalManagementService.saveImportHistory(responseList, animalRequestUpload.getFormat(),
				animalRequestUpload.getUploadedBy(),headers,header);
		if (Format.ANIMAL.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			animalManagementService.saveAnimalList(responseList,headers);

		} else if (Format.CBC.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			animalManagementService.saveAnimalTestResults(responseList,headers);

		} else if (Format.ELISA.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{

		}

		return result;

	}

	@RequestMapping(value = "/importFileHistoryList", method = RequestMethod.GET)
	public Iterable<ImportFileHistory> getImportFileHistory()
	{
		return animalManagementService.getImportFileHistory();
	}

	@RequestMapping(value = "/importFileHistoryList/{id}", method = RequestMethod.GET)
	public List<String> getResponseFile(@PathVariable String id)
	{

		return animalManagementService.getResponseFile(id);
	}

	/**
	 * getAnimal method returns the animal detail for given id.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Animals getAnimal(@PathVariable("id") Long id)
	{
		return animalManagementService.getAnimal(id);
	}

	/**
	 * getAnimalsList method returns the all animals.
	 * 
	 * @return List<{@link Animals}>
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Animals> getAnimalsList()
	{
		return animalManagementService.getAnimalsList();
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createAnimals(@RequestBody Animals animals)
	{
		AnimalsValidator animalsValidator = new AnimalsValidator();
		
		 Map<String, List<String>> map = constantsMap.loadConstants();
		 List<String> species = map.get("animals.species.type");
		 
//		Iterable<Species> species =  animalManagementService.getAllSpecies();
		List<String> errorCodes = animalsValidator.validateForAnimalCreate(animals,species);
		if (!errorCodes.isEmpty())
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(errorCodes.toArray(new String[errorCodes.size()])),
					HttpStatus.INTERNAL_SERVER_ERROR);

		} else
		{
			Animals animal = animalManagementService.createAnimal(animals);
			return new ResponseEntity<Object>(animal, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Animals updateAnimals(@RequestBody Animals animals)
	{
		return animalManagementService.createAnimal(animals);
	}

	@RequestMapping(value = "/animalTests", method = RequestMethod.GET)
	public Iterable<AnimalTest> getAllAnimalTests()
	{
		return animalManagementService.getAllAnimalTests();
	}

	@RequestMapping(value = "/animalTests/{animaltestid}", method = RequestMethod.GET)
	public AnimalTest getAnimalTest(@PathVariable("animaltestid") Long animalTestId)
	{
		return animalManagementService.getAnimalTest(animalTestId);
	}

	@RequestMapping(value = "/animalTestsResults", method = RequestMethod.GET)
	public Iterable<AnimalTestResults> getAnimalTestResults()
	{
		return animalManagementService.getAnimalTestResults();
	}

	@RequestMapping(value = "/animalTestsResults/{animaltestresultid}", method = RequestMethod.GET)
	public AnimalTestResults getAnimalTestResultById(@PathVariable("animaltestresultid") Long animalTestResultId)
	{
		return animalManagementService.getAnimalTestResultById(animalTestResultId);
	}

	/*@RequestMapping(value = "/animalTestsResults/{startdate}/{enddate}", method = RequestMethod.GET)
	public Iterable<AnimalTestResults> getAnimalTestResultsForDateRange(
			@PathVariable("startdate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@PathVariable("enddate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate)
	{
		return animalManagementService.getAnimalTestResultsForDateRange(startDate, endDate);
	}*/
	
	@RequestMapping(value = "/animalTestsResults/report", method = RequestMethod.PUT)
	public Iterable<AnimalTestResults> getAnimalTestResultsForDateRange(@RequestBody AnimalTestResultDTO animalTestResultDTO)
	{

		return animalManagementService.getCBCTestResults(animalTestResultDTO);
	}

	@RequestMapping(value = "/speciesrangedetails", method = RequestMethod.GET)
	public Iterable<SpeciesDetail> getSpeciesRangeDetails()
	{
		return animalManagementService.getSpeciesRangeDetails();
	}

	@RequestMapping(value = "/speciesrangedetails/{id}", method = RequestMethod.GET)
	public SpeciesDetail getSpeciesRangeDetails(@PathVariable("id") Long id)
	{
		return animalManagementService.getSpeciesRangeDetails(id);
	}

	@RequestMapping(value = "/animalTestsResults/animals/{animalid}", method = RequestMethod.GET)
	public Iterable<AnimalTestResults> getAnimalTestResultsForAnimal(@PathVariable("animalid") Long animalId)
	{
		return animalManagementService.getAnimalTestResultsForAnimal(animalId);
	}

	@RequestMapping(value = "/elisa", method = RequestMethod.POST)
	public ResponseEntity<Object> addElisa(@RequestBody ELISATestDTO elisaTestDto)
	{
		AnimalsValidator animalsValidator = new AnimalsValidator();
		List<String> errorCodes = animalsValidator.validateElisaTest(elisaTestDto);
		if (errorCodes.isEmpty())
		{
			ELISATest elisaTest = animalManagementService.addElisa(elisaTestDto);
			return new ResponseEntity<Object>(elisaTest, HttpStatus.OK);
		} else
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(errorCodes.toArray(new String[errorCodes.size()])),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/mamu", method = RequestMethod.POST)
	public ResponseEntity<Object> addMamu(@RequestBody MamuTestDTO mamuTestDto)
	{
		AnimalsValidator animalsValidator = new AnimalsValidator();
		List<String> errorCodes = animalsValidator.validateMamuTest(mamuTestDto);
		if (errorCodes.isEmpty())
		{
			MamuTest mamuTest = animalManagementService.addMamu(mamuTestDto);
			return new ResponseEntity<Object>(mamuTest, HttpStatus.OK);
		} else
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(errorCodes.toArray(new String[errorCodes.size()])),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/elisa", method = RequestMethod.PUT)
	public ELISATest updateMamu(@RequestBody ELISATestDTO elisaTestDto)
	{
		return animalManagementService.updateElisa(elisaTestDto);
	}

	@RequestMapping(value = "/mamu", method = RequestMethod.PUT)
	public MamuTest updateMamu(@RequestBody MamuTestDTO mamuTestDto)
	{
		return animalManagementService.updateMamu(mamuTestDto);
	}

	@RequestMapping(value = "/elisa/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteElisa(@PathVariable("id") Long id)
	{
		try
		{
			animalManagementService.deleteElisa(id);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception e)
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(Messages.properties.getProperty("elisa.not.exist")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/mamu/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteMamu(@PathVariable("id") Long id)
	{
		try
		{
			animalManagementService.deleteMamu(id);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception e)
		{
			e.printStackTrace();
			return new ResponseEntity<Object>(new ARDXXErrorPayload(Messages.properties.getProperty("mamu.not.exist")),
					HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@RequestMapping(value = "/viruslist", method = RequestMethod.GET)
	public Iterable<Virus> getAllViruses()
	{
		return animalManagementService.getAllViruses();
	}

	@RequestMapping(value = "/mamulist", method = RequestMethod.GET)
	public Iterable<MamuList> getAllMamuList()
	{
		return animalManagementService.getAllMamuList();
	}

	@RequestMapping(value = "/elisalist/{animalId}", method = RequestMethod.GET)
	public Iterable<ELISATest> getElisaTestByAnimalId(@PathVariable("animalId") String animalId)
	{
		return animalManagementService.getElisaTestByAnimalId(animalId);
	}

	@RequestMapping(value = "/mamulist/{animalId}", method = RequestMethod.GET)
	public Iterable<MamuTest> getMamuTestByAnimalId(@PathVariable("animalId") String animalId)
	{
		return animalManagementService.getMamuTestByAnimalId(animalId);
	}
	
	@RequestMapping(value = "/species", method = RequestMethod.GET)
	public List<String> getAllSpecies()
	{
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> species = map.get("animals.species.type");
		return species;
	}
	
	@RequestMapping(value = "/{animalid}/history", method = RequestMethod.GET)
	public Iterable<AnimalHistory> getAnimalHistory(@PathVariable("animalid") Long animalId)
	{
		return animalManagementService.getAnimalHistory(animalId);
	}
	
	@RequestMapping(value = "/status/{status}", method = RequestMethod.GET)
	public List<Animals> getAnimalByStatus(@PathVariable("status") String status)
	{
		return animalManagementService.getAnimalByStatus(status);
	}
	
	@RequestMapping(value = "/contractornumber", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureType()
	{
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> contractorNumber = map.get("animals.contractor.number");
		return new ResponseEntity<Object>(contractorNumber, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/bulkedit", method = RequestMethod.PUT)
	public ResponseEntity<Object> getAnimalByStatus(@RequestBody BulkEditDTO bulkEditDTO)
	{
		try
		{
			List<Animals> animals = animalManagementService.animalBulkEdit(bulkEditDTO);
			return new ResponseEntity<Object>(animals, HttpStatus.OK);
		} catch (ARDXXAppException exception)
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);

		} catch (Exception exception)
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getLocalizedMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

}
