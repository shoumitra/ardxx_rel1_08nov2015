package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;

@Repository
public interface AnimalHistoryRepository extends PagingAndSortingRepository<AnimalHistory, Long>
{
	@Query(nativeQuery=true, value="SELECT  a.*  FROM  animal_history a where a.animal_table_id =:animalTableId ORDER BY added_date DESC LIMIT 10;")
	public Iterable<AnimalHistory> findByAnimalTableId(@Param("animalTableId") Long animalId);
	
	@Query(nativeQuery=true, value="SELECT  a.*  FROM  animal_history a INNER JOIN ( SELECT  animal_table_id, MAX(added_date) added_date FROM    animal_history where schedule_id=:scheduleId  GROUP   BY animal_table_id ) b ON  a.animal_table_id = b.animal_table_id AND a.added_date = b.added_date;")
	public List<AnimalHistory> findDistinctAnimalTableIdByScheduleIdOrderByAddedDateDesc(@Param("scheduleId") Long scheduleId);
}
