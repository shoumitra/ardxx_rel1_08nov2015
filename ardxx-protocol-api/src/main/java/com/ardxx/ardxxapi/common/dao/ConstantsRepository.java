package com.ardxx.ardxxapi.common.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.common.domain.Constants;

/**
 * 
 * @author Raj
 *
 */
@Repository
public interface ConstantsRepository extends PagingAndSortingRepository<Constants, Long>
{
	public List<Constants> findByNameStartingWith(@Param("name") String name);

	public List<Constants> findByName(@Param("name") String name);
}
