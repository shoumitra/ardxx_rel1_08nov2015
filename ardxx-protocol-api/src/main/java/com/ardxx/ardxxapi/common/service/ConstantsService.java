package com.ardxx.ardxxapi.common.service;

import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.common.domain.Constants;

/**
 * 
 * @author Raj
 *
 */
@Service
public interface ConstantsService
{
	public Iterable<Constants> getAllConstants();
}
