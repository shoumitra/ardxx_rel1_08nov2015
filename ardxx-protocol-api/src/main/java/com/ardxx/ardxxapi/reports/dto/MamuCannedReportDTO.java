package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.Date;

public class MamuCannedReportDTO implements Serializable
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String animal;
	private String sex;
	private String a01;
	private Date dob;
	private String weight;
	private String file;
	private String protocol;
	public String getAnimal()
	{
		return animal;
	}
	public void setAnimal(String animal)
	{
		this.animal = animal;
	}
	public String getSex()
	{
		return sex;
	}
	public void setSex(String sex)
	{
		this.sex = sex;
	}
	public String getA01()
	{
		return a01;
	}
	public void setA01(String a01)
	{
		this.a01 = a01;
	}
	public Date getDob()
	{
		return dob;
	}
	public void setDob(Date dob)
	{
		this.dob = dob;
	}
	public String getWeight()
	{
		return weight;
	}
	public void setWeight(String weight)
	{
		this.weight = weight;
	}
	public String getFile()
	{
		return file;
	}
	public void setFile(String file)
	{
		this.file = file;
	}
	public String getProtocol()
	{
		return protocol;
	}
	public void setProtocol(String protocol)
	{
		this.protocol = protocol;
	}
	
	
}
