package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.List;

public class QueryBuilder implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<ReportDTO> entities;
	private List<String> columnOrder;
	private String condition;

	public List<ReportDTO> getEntities()
	{
		return entities;
	}

	public void setEntities(List<ReportDTO> entities)
	{
		this.entities = entities;
	}

	public List<String> getColumnOrder()
	{
		return columnOrder;
	}

	public void setColumnOrder(List<String> columnOrder)
	{
		this.columnOrder = columnOrder;
	}

	public String getCondition()
	{
		return condition;
	}

	public void setCondition(String condition)
	{
		this.condition = condition;
	}

}
