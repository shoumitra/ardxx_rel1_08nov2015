package com.ardxx.ardxxapi.reports.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.animalmanagement.controller.validator.AnimalsValidator;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.reports.dao.ReportsDAO;
import com.ardxx.ardxxapi.reports.dto.QueryBuilder;

@Service
public class ReportsServiceImpl implements ReportsService
{

	@Autowired
	ReportsDAO reportsDAO;

	@Override
	public List<Map<String, Object>> getReports(QueryBuilder queryBuilder)
	{
		return reportsDAO.getReports(queryBuilder);
	}

	@Override
	public List<?> getCannedReports(final Long id, final String searchId, final Date fromDate, final Date toDate) throws Exception
	{

		if (id == 1)
		{
			String animalId = searchId;

			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, animalId, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}

			return reportsDAO.getRoomHistoryDetailsOfAnimal(animalId);

		} else if (id == 2)
		{
			String roomId = searchId;
			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, roomId, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}
			return reportsDAO.getAnimalDetailsInRoom(roomId);
		} else if (id == 3)
		{
			String studyId = searchId;
			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, studyId, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}
			return reportsDAO.getCbcCannedReport(studyId);

		} else if (id == 4)
		{
			String studyId = searchId;
			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, studyId, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}
			return reportsDAO.getAnimalWeights(studyId);

		} else if (id == 5)
		{
			String jobCode = searchId;
			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, jobCode, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}
			return reportsDAO.getJobCodeDetails(jobCode, fromDate, toDate);

		} else if (id == 6)
		{
			String animalId = searchId;
			AnimalsValidator animalsValidator = new AnimalsValidator();
			List<String> errorCodes = animalsValidator.validateCannedQuery(id, animalId, fromDate, toDate);
			if (errorCodes != null && !errorCodes.isEmpty())
			{
				ARDXXAppException ardxxAppException = new ARDXXAppException();
				ardxxAppException.setErrorCodes(errorCodes);
				throw ardxxAppException;
			}
			return reportsDAO.getAnimalTreatmentDetails(animalId);

		} else if (id == 7)
		{
			return reportsDAO.getRhesusAnimalDetails();

		} else
		{
			return null;
		}

	}

	@Override
	public List<?> getReportList()
	{

		return reportsDAO.getReportList();
	}
}
