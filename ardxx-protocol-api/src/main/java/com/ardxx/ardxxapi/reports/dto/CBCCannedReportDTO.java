package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.Date;

public class CBCCannedReportDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String animalId;
	private Date collectedDate;
	private String wbc;
	private String rbc;
	private String hgb;
	private String hct;
	private String mcv;
	private String mch;
	private String mchc;
	private String nrbc;
	private String neutrophilSeg;
	private String neutrophilBand;
	private String lymphocyte;
	private String monocyte;
	private String eosinophil;
	private String basophil;
	private String metamyelocyte;
	private String myelocyte;
	private String promyelocyte;
	private String plateletEstimate;
	private String polychromasia;
	private String anisocytosis;
	private String poikilocytosis;
	private String heinzBodies;
	private String remarks;
	private String absoluteNeutrophilSeg;
	private String absoluteNeutrophilBand;
	private String absoluteLymphocyte;
	private String absoluteMonocyte;
	private String absoluteEosinophil;
	private String absoluteBasophil;
	private String absoluteMetamyelocytes;
	private String absolutePromyelocytes;
	private String plateletCount;

	public String getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(String animalId)
	{
		this.animalId = animalId;
	}

	public Date getCollectedDate()
	{
		return collectedDate;
	}

	public void setCollectedDate(Date collectedDate)
	{
		this.collectedDate = collectedDate;
	}

	public String getWbc()
	{
		return wbc;
	}

	public void setWbc(String wbc)
	{
		this.wbc = wbc;
	}

	public String getRbc()
	{
		return rbc;
	}

	public void setRbc(String rbc)
	{
		this.rbc = rbc;
	}

	public String getHgb()
	{
		return hgb;
	}

	public void setHgb(String hgb)
	{
		this.hgb = hgb;
	}

	public String getHct()
	{
		return hct;
	}

	public void setHct(String hct)
	{
		this.hct = hct;
	}

	public String getMcv()
	{
		return mcv;
	}

	public void setMcv(String mcv)
	{
		this.mcv = mcv;
	}

	public String getMch()
	{
		return mch;
	}

	public void setMch(String mch)
	{
		this.mch = mch;
	}

	public String getMchc()
	{
		return mchc;
	}

	public void setMchc(String mchc)
	{
		this.mchc = mchc;
	}

	public String getNrbc()
	{
		return nrbc;
	}

	public void setNrbc(String nrbc)
	{
		this.nrbc = nrbc;
	}

	public String getNeutrophilSeg()
	{
		return neutrophilSeg;
	}

	public void setNeutrophilSeg(String neutrophilSeg)
	{
		this.neutrophilSeg = neutrophilSeg;
	}

	public String getNeutrophilBand()
	{
		return neutrophilBand;
	}

	public void setNeutrophilBand(String neutrophilBand)
	{
		this.neutrophilBand = neutrophilBand;
	}

	public String getLymphocyte()
	{
		return lymphocyte;
	}

	public void setLymphocyte(String lymphocyte)
	{
		this.lymphocyte = lymphocyte;
	}

	public String getMonocyte()
	{
		return monocyte;
	}

	public void setMonocyte(String monocyte)
	{
		this.monocyte = monocyte;
	}

	public String getEosinophil()
	{
		return eosinophil;
	}

	public void setEosinophil(String eosinophil)
	{
		this.eosinophil = eosinophil;
	}

	public String getBasophil()
	{
		return basophil;
	}

	public void setBasophil(String basophil)
	{
		this.basophil = basophil;
	}

	public String getMetamyelocyte()
	{
		return metamyelocyte;
	}

	public void setMetamyelocyte(String metamyelocyte)
	{
		this.metamyelocyte = metamyelocyte;
	}

	public String getMyelocyte()
	{
		return myelocyte;
	}

	public void setMyelocyte(String myelocyte)
	{
		this.myelocyte = myelocyte;
	}

	public String getPromyelocyte()
	{
		return promyelocyte;
	}

	public void setPromyelocyte(String promyelocyte)
	{
		this.promyelocyte = promyelocyte;
	}

	public String getPlateletEstimate()
	{
		return plateletEstimate;
	}

	public void setPlateletEstimate(String plateletEstimate)
	{
		this.plateletEstimate = plateletEstimate;
	}

	public String getPolychromasia()
	{
		return polychromasia;
	}

	public void setPolychromasia(String polychromasia)
	{
		this.polychromasia = polychromasia;
	}

	public String getAnisocytosis()
	{
		return anisocytosis;
	}

	public void setAnisocytosis(String anisocytosis)
	{
		this.anisocytosis = anisocytosis;
	}

	public String getPoikilocytosis()
	{
		return poikilocytosis;
	}

	public void setPoikilocytosis(String poikilocytosis)
	{
		this.poikilocytosis = poikilocytosis;
	}

	public String getHeinzBodies()
	{
		return heinzBodies;
	}

	public void setHeinzBodies(String heinzBodies)
	{
		this.heinzBodies = heinzBodies;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getAbsoluteNeutrophilSeg()
	{
		return absoluteNeutrophilSeg;
	}

	public void setAbsoluteNeutrophilSeg(String absoluteNeutrophilSeg)
	{
		this.absoluteNeutrophilSeg = absoluteNeutrophilSeg;
	}

	public String getAbsoluteNeutrophilBand()
	{
		return absoluteNeutrophilBand;
	}

	public void setAbsoluteNeutrophilBand(String absoluteNeutrophilBand)
	{
		this.absoluteNeutrophilBand = absoluteNeutrophilBand;
	}

	public String getAbsoluteLymphocyte()
	{
		return absoluteLymphocyte;
	}

	public void setAbsoluteLymphocyte(String absoluteLymphocyte)
	{
		this.absoluteLymphocyte = absoluteLymphocyte;
	}

	public String getAbsoluteMonocyte()
	{
		return absoluteMonocyte;
	}

	public void setAbsoluteMonocyte(String absoluteMonocyte)
	{
		this.absoluteMonocyte = absoluteMonocyte;
	}

	public String getAbsoluteEosinophil()
	{
		return absoluteEosinophil;
	}

	public void setAbsoluteEosinophil(String absoluteEosinophil)
	{
		this.absoluteEosinophil = absoluteEosinophil;
	}

	public String getAbsoluteBasophil()
	{
		return absoluteBasophil;
	}

	public void setAbsoluteBasophil(String absoluteBasophil)
	{
		this.absoluteBasophil = absoluteBasophil;
	}

	public String getAbsoluteMetamyelocytes()
	{
		return absoluteMetamyelocytes;
	}

	public void setAbsoluteMetamyelocytes(String absoluteMetamyelocytes)
	{
		this.absoluteMetamyelocytes = absoluteMetamyelocytes;
	}

	public String getAbsolutePromyelocytes()
	{
		return absolutePromyelocytes;
	}

	public void setAbsolutePromyelocytes(String absolutePromyelocytes)
	{
		this.absolutePromyelocytes = absolutePromyelocytes;
	}

	public String getPlateletCount()
	{
		return plateletCount;
	}

	public void setPlateletCount(String plateletCount)
	{
		this.plateletCount = plateletCount;
	}

}
