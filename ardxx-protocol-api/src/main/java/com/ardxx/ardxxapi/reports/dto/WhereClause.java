package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.Date;

public class WhereClause implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String column;
	private String value;
	private String otherValue;
	private String type;

	public String getColumn()
	{
		return column;
	}

	public void setColumn(String column)
	{
		this.column = column;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getOtherValue()
	{
		return otherValue;
	}

	public void setOtherValue(String otherValue)
	{
		this.otherValue = otherValue;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

}
