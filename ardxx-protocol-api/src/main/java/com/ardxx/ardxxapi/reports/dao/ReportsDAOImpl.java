package com.ardxx.ardxxapi.reports.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.BloodSample;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.common.ReportUtil;
import com.ardxx.ardxxapi.common.dao.ConstantsRepository;
import com.ardxx.ardxxapi.common.domain.Constants;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.reports.dto.AnimalHistoryCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.AnimalJobCodeDetailReportsDTO;
import com.ardxx.ardxxapi.reports.dto.AnimalTreatmentCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.AnimalWeightReportsDTO;
import com.ardxx.ardxxapi.reports.dto.CBCCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.ConstantReportDTO;
import com.ardxx.ardxxapi.reports.dto.MamuCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.QueryBuilder;
import com.ardxx.ardxxapi.reports.dto.ReportDTO;
import com.ardxx.ardxxapi.reports.dto.RoomHistoryCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.WhereClause;

@Repository
public class ReportsDAOImpl implements ReportsDAO, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	ConstantsRepository constantsRepository;

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Map<String, Object>> getReports(QueryBuilder queryBuilder)
	{
		List<ReportDTO> reportDTOs = queryBuilder.getEntities();
		List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();
		
		String condition = null;
		
		if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.AND))
		{
			condition = ReportUtil.AND;
		}
		else if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.OR))
		{
			condition = ReportUtil.OR;
		}		
		
		List<String> responseColumns = new ArrayList<String>();

		CriteriaBuilder qb = entityManager.getCriteriaBuilder();

		ReportDTO study = new ReportDTO();
		study.setTable(ReportUtil.STUDY);
		boolean isStudy = false;

		ReportDTO procedure = new ReportDTO();
		procedure.setTable(ReportUtil.PROCEDURE);
		boolean isProcedure = false;

		ReportDTO schedule = new ReportDTO();
		schedule.setTable(ReportUtil.SCHEDULE);
		boolean isSchedule = false;

		ReportDTO animal = new ReportDTO();
		animal.setTable(ReportUtil.ANIMALS);
		boolean isAnimal = false;

		ReportDTO protocol = new ReportDTO();
		protocol.setTable(ReportUtil.PROTOCOL);
		boolean isProtocol = false;

		ReportDTO animalHistory = new ReportDTO();
		animalHistory.setTable(ReportUtil.ANIMAL_HISTORY);
		boolean isAnimalHistory = false;

		CriteriaQuery cq = null;
		Root root = null;
		ReportDTO reportDTO = null;
		List<Path> columnList = new ArrayList<Path>();
		List<Predicate> whereCondition = new ArrayList<Predicate>();
		boolean hasPredicate = false;
		boolean hasClinicalPredicate = false;
		int columnIndex = 0;

		Predicate clinicalPredicate = null;
		Predicate draftPredicate = null;
		
		
		if (reportDTOs.contains(animalHistory) && !reportDTOs.contains(study) && !reportDTOs.contains(procedure))
		{
			cq = qb.createQuery(AnimalHistory.class);
			root = cq.from(AnimalHistory.class);
			for (ReportDTO dTO : reportDTOs)
			{
				if (dTO.equals(animalHistory))
				{
					reportDTO = dTO;
					for (String columns : reportDTO.getColumns())
					{
						columnList.add(root.get(columns));
						responseColumns.add(columnIndex, ReportUtil.ANIMAL_HISTORY + "." + columns);
						columnIndex++;

					}
					break;
				}
			}

			if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
			{

				for (WhereClause where : reportDTO.getWhereClause())
				{
					whereForRootClass(qb, root, whereCondition, where);
				}
			}

			isAnimalHistory = true;
		} else if (reportDTOs.contains(animalHistory) && reportDTOs.contains(study) && !reportDTOs.contains(procedure))
		{
			return studyAnimalHistory(reportDTOs, animalHistory, study, responseColumns, queryBuilder, response, procedure, false,condition);
		} else if (reportDTOs.contains(animalHistory) && reportDTOs.contains(procedure))
		{
			return studyAnimalHistory(reportDTOs, animalHistory, study, responseColumns, queryBuilder, response, procedure, true,condition);
		}
		if (!isAnimalHistory && reportDTOs.contains(study))
		{
			cq = qb.createQuery(Study.class);
			root = cq.from(Study.class);
			
			if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.AND))
			{
				whereCondition.add(qb.equal(root.get("isDraft"), false));
				whereCondition.add(qb.equal(root.get("isClinical"), false));
			}
			else if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.OR))
			{
				clinicalPredicate = qb.equal(root.get("isClinical"), false);
				draftPredicate = qb.equal(root.get("isDraft"), false);
			}
			
			
			hasClinicalPredicate = true;
			for (ReportDTO dTO : reportDTOs)
			{
				if (dTO.equals(study))
				{
					reportDTO = dTO;
					for (String columns : reportDTO.getColumns())
					{
						columnList.add(root.get(columns));
						responseColumns.add(columnIndex, ReportUtil.STUDY + "." + columns);
						columnIndex++;

					}
					break;
				}
			}

			if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
			{

				for (WhereClause where : reportDTO.getWhereClause())
				{
					whereForRootClass(qb, root, whereCondition, where);
				}
			}

			isStudy = true;
		}

		Join<Study, Protocol> protocolJoin = null;

		if (!isAnimalHistory && reportDTOs.contains(protocol))
		{
			if (isStudy)
			{
				protocolJoin = root.join("protocol");
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(protocol))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(protocolJoin.get(columns));
							responseColumns.add(columnIndex, "protocol." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(protocolJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			} else
			{
				cq = qb.createQuery(Protocol.class);
				root = cq.from(Protocol.class);
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(protocol))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(root.get(columns));
							responseColumns.add(columnIndex, "protocol." + columns);
							columnIndex++;

						}
						break;
					}
				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForRootClass(qb, root, whereCondition, where);
					}
				}

			}
		}

		Join<StudyProcedure, Study> procedureJoin = null;

		if (!isAnimalHistory && reportDTOs.contains(procedure))
		{
			List<String> procedureAnimals = new ArrayList<String>();
			List<WhereClause> procedureAnimalsWhere = new ArrayList<WhereClause>();
			List<String> bloodSamples = new ArrayList<String>();
			List<WhereClause> bloodSamplesWhere = new ArrayList<WhereClause>();
			if (isStudy)
			{
				procedureJoin = root.join("procedures");

				columnIndex = setStudyProcedureJoins(reportDTOs, procedure, reportDTO, procedureAnimals, bloodSamples, columnList, procedureJoin,
						null, responseColumns, columnIndex, procedureAnimalsWhere, bloodSamplesWhere, whereCondition, qb);

			} else
			{
				cq = qb.createQuery(StudyProcedure.class);
				root = cq.from(StudyProcedure.class);

				columnIndex = setStudyProcedureJoins(reportDTOs, procedure, reportDTO, procedureAnimals, bloodSamples, columnList, null, root,
						responseColumns, columnIndex, procedureAnimalsWhere, bloodSamplesWhere, whereCondition, qb);

			}

			isProcedure = true;

		}

		if (!isAnimalHistory && reportDTOs.contains(schedule))
		{
			for (ReportDTO dTO : reportDTOs)
			{
				if (dTO.equals(schedule))
				{
					reportDTO = dTO;
					break;
				}
			}

			Join<ProcedureSchedule, Study> scheduleJoin = null;
			if (isStudy && !isProcedure)
			{
				scheduleJoin = root.join("procedures").join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (!isStudy && isProcedure)
			{
				scheduleJoin = root.join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (isStudy && isProcedure)
			{
				scheduleJoin = procedureJoin.join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (!isStudy && !isProcedure)
			{
				cq = qb.createQuery(ProcedureSchedule.class);
				root = cq.from(ProcedureSchedule.class);

				for (String columns : reportDTO.getColumns())
				{
					columnList.add(root.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}

			if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
			{

				for (WhereClause where : reportDTO.getWhereClause())
				{
					if (scheduleJoin != null)
					{
						whereForJoin(scheduleJoin, whereCondition, qb, where, where.getColumn());
					} else
					{
						whereForRootClass(qb, root, whereCondition, where);
					}

				}
			}
			isSchedule = true;
		}

		if (!isAnimalHistory && reportDTOs.contains(animal))
		{
			if (isProcedure)
			{

				Join<StudyProcedure, Animals> procedureAnimalJoin = root.join("animals");
				;
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(procedureAnimalJoin.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(procedureAnimalJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			} else if (isStudy)
			{
				Join<Study, Animals> studyAnimalsJoin = null;
				if (isProcedure)
				{
					studyAnimalsJoin = procedureJoin.join("animals");
				} else
				{
					studyAnimalsJoin = root.join("animals");
				}

				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(studyAnimalsJoin.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(studyAnimalsJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			}

			else
			{
				cq = qb.createQuery(Animals.class);
				root = cq.from(Animals.class);
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(root.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}
				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForRootClass(qb, root, whereCondition, where);
					}
				}
			}

			isAnimal = true;

		}

		Path[] paths = new Path[columnList.size()];

		for (int i = 0; i < columnList.size(); i++)
		{
			paths[i] = columnList.get(i);
		}

		Predicate[] predicates = new Predicate[whereCondition.size()];

		for (int i = 0; i < whereCondition.size(); i++)
		{
			predicates[i] = whereCondition.get(i);
			hasPredicate = true;
		}

		cq.select(qb.array(paths));
		
		
		if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.AND))
		{
			cq.where(predicates);
		}
		else if(queryBuilder.getCondition().equalsIgnoreCase(ReportUtil.OR))
		{
			if(hasClinicalPredicate && !hasPredicate)
			{
				Predicate ignore = qb.and(clinicalPredicate,draftPredicate);
				cq.where(ignore);
			}
			else if(hasClinicalPredicate && hasPredicate)
			{
				Predicate ignore = qb.and(clinicalPredicate,draftPredicate);
				Predicate or = qb.or(predicates);
				cq.where(ignore,or);
			}
			else if(!hasClinicalPredicate && hasPredicate)
			{
				Predicate or = qb.or(predicates);
				cq.where(or);
			}

		}
		
		TypedQuery<Object> typedQuery = entityManager.createQuery(cq);
		List<Object> resultlist = typedQuery.getResultList();
		for (Object object : resultlist)
		{
			LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
			if (object != null)
			{
				if (object instanceof Object[])
				{
					Object[] resultArray = (Object[]) object;

					for (String string : queryBuilder.getColumnOrder())
					{
						for (int i = 0; i < resultArray.length; i++)
						{

							if (string.equalsIgnoreCase(responseColumns.get(i)))
							{
								Object obj = resultArray[i];

								if (obj instanceof Date)
								{
									Date date = (Date) obj;
									row.put(responseColumns.get(i), new SimpleDateFormat("MM/dd/yyyy").format(date));
								} else
								{
									row.put(responseColumns.get(i), resultArray[i]);
								}

								break;
							}

						}
					}
				} else if (object instanceof String)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else if (object instanceof Long)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				}
			}

			response.add(row);
		}

		return response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private int setStudyProcedureJoins(List<ReportDTO> reportDTOs, ReportDTO procedure, ReportDTO reportDTO, List<String> procedureAnimals,
			List<String> bloodSamples, List<Path> columnList, Join join, Root root, List<String> responseColumns, int columnIndex,
			List<WhereClause> procedureAnimalsWhere, List<WhereClause> bloodSamplesWhere, List<Predicate> whereCondition, CriteriaBuilder qb)
	{
		for (ReportDTO dTO : reportDTOs)
		{
			if (dTO.equals(procedure))
			{
				reportDTO = dTO;
				for (String columns : reportDTO.getColumns())
				{
					if (columns.startsWith("bloodSample"))
					{
						bloodSamples.add(columns);
					} else
					{
						if (root != null)
						{
							columnList.add(root.get(columns));
						} else
						{
							columnList.add(join.get(columns));
						}

						responseColumns.add(columnIndex, "procedure." + columns);
						columnIndex++;
					}

				}
				break;
			}
		}

		if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
		{
			for (WhereClause where : reportDTO.getWhereClause())
			{
				if (where.getColumn().startsWith("bloodSample"))
				{
					bloodSamplesWhere.add(where);
				} else
				{
					if (root != null)
					{
						whereForRootClass(qb, root, whereCondition, where);
					} else
					{
						whereForJoin(join, whereCondition, qb, where, where.getColumn());
					}

				}

			}
		}

		Join<StudyProcedure, BloodSample> bloodSamplesJoin = null;
		if (!bloodSamples.isEmpty())
		{
			if (root != null)
			{
				bloodSamplesJoin = root.join("bloodSample");
			} else
			{
				bloodSamplesJoin = join.join("bloodSample");
			}

			for (String string : bloodSamples)
			{
				string = string.replace("bloodSample.", "");
				columnList.add(bloodSamplesJoin.get(string));
				responseColumns.add(columnIndex, "procedure.bloodSample." + string);
				columnIndex++;
			}
		}

		if (!bloodSamples.isEmpty())
		{
			if (bloodSamplesJoin == null)
			{
				if (root != null)
				{
					bloodSamplesJoin = root.join("bloodSample");
				} else
				{
					bloodSamplesJoin = join.join("bloodSample");
				}

			}

			for (WhereClause where : bloodSamplesWhere)
			{
				String column = where.getColumn().replace("bloodSample.", "");
				whereForJoin(bloodSamplesJoin, whereCondition, qb, where, column);
			}
		}

		return columnIndex;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void whereForJoin(Join join, List<Predicate> whereCondition, CriteriaBuilder qb, WhereClause where, String column)
	{
		if (where.getType().equalsIgnoreCase("EQUAL"))
		{
			whereCondition.add(qb.equal(join.get(column), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("ENDSWITH"))
		{
			whereCondition.add(qb.like(join.get(column).as(String.class), "%" + where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("STARTSWITH"))
		{
			whereCondition.add(qb.like(join.get(column).as(String.class), where.getValue() + "%"));
		}
		if (where.getType().equalsIgnoreCase("BETWEEN"))
		{
			boolean isNumber;
			boolean isDate;

			Long value1 = null;
			Long value2 = null;

			Date fromDate = null;
			Date toDate = null;

			try
			{
				value1 = Long.parseLong(where.getValue());
				value2 = Long.parseLong(where.getOtherValue());
				isNumber = true;
			} catch (NumberFormatException e)
			{
				isNumber = false;
			}

			try
			{
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				fromDate = format.parse(where.getValue());
				toDate = format.parse(where.getOtherValue());
				isDate = true;
			} catch (Exception e)
			{
				isDate = false;
			}

			if (isNumber)
			{
				whereCondition.add(qb.between(join.get(column), value1, value2));
			} else if (isDate)
			{
				whereCondition.add(qb.between(join.get(column), fromDate, toDate));
			}

		}
		if (where.getType().equalsIgnoreCase("GREATERTHAN"))
		{
			whereCondition.add(qb.greaterThan(join.get(column), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("LESSTHAN"))
		{
			whereCondition.add(qb.lessThan(join.get(column), where.getValue()));
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void whereForRootClass(CriteriaBuilder qb, Root root, List<Predicate> whereCondition, WhereClause where)
	{
		if (where.getType().equalsIgnoreCase("EQUAL"))
		{
			whereCondition.add(qb.equal(root.get(where.getColumn()), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("STARTSWITH"))
		{
			
			whereCondition.add(qb.like(root.get(where.getColumn()).as(String.class), where.getValue() + "%"));
		}
		if (where.getType().equalsIgnoreCase("ENDSWITH"))
		{
			
			whereCondition.add(qb.like(root.get(where.getColumn()).as(String.class), "%" + where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("BETWEEN"))
		{

			boolean isNumber;
			boolean isDate;

			Long value1 = null;
			Long value2 = null;

			Date fromDate = null;
			Date toDate = null;

			try
			{
				value1 = Long.parseLong(where.getValue());
				value2 = Long.parseLong(where.getOtherValue());
				isNumber = true;
			} catch (NumberFormatException e)
			{
				isNumber = false;
			}

			try
			{
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				fromDate = format.parse(where.getValue());
				toDate = format.parse(where.getOtherValue());
				isDate = true;
			} catch (Exception e)
			{
				isDate = false;
			}

			if (isNumber)
			{
				whereCondition.add(qb.between(root.get(where.getColumn()), value1, value2));
			} else if (isDate)
			{
				whereCondition.add(qb.between(root.get(where.getColumn()), fromDate, toDate));
			}
		}
		if (where.getType().equalsIgnoreCase("GREATERTHAN"))
		{
			whereCondition.add(qb.greaterThan(root.get(where.getColumn()), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("LESSTHAN"))
		{
			whereCondition.add(qb.lessThan(root.get(where.getColumn()), where.getValue()));
		}
	}

	/*
	 * @Override public List<?> getRoomHistoryDetailsOfAnimal(final String animalId) {
	 * 
	 * Query query = entityManager.createQuery("SELECT DISTINCT roomNum,recievedDate,departedDate FROM AnimalHistory WHERE animalId=?");
	 * query.setParameter(1, animalId); List<Object[]> historyList = query.getResultList(); List<RoomHistoryCannedReportDTO> roomHistoryCannedReports
	 * = new ArrayList<RoomHistoryCannedReportDTO>();
	 * 
	 * for (Object[] list : historyList) { RoomHistoryCannedReportDTO roomHistoryCannedReportDTO = new RoomHistoryCannedReportDTO(); if (list != null
	 * && list.length > 0) { roomHistoryCannedReportDTO.setRoom(list[0] == null ? "" : list[0].toString()); if (list.length > 1) { if (list[1]
	 * instanceof Date) { roomHistoryCannedReportDTO.setDateEntered((Date) list[1]); }
	 * 
	 * } if (list.length > 2) { if (list[2] instanceof Date) { roomHistoryCannedReportDTO.setDateExit((Date) list[2]); }
	 * 
	 * } }
	 * 
	 * roomHistoryCannedReports.add(roomHistoryCannedReportDTO); }
	 * 
	 * return roomHistoryCannedReports; }
	 */

	@Override
	public List<?> getRoomHistoryDetailsOfAnimal(final String animalId)
	{

		Query query = entityManager.createNativeQuery("SELECT room_num ,added_date FROM animal_history WHERE animal_id=?  order by added_date ASC");
		query.setParameter(1, animalId);
		List<Object[]> historyList = query.getResultList();
		List<RoomHistoryCannedReportDTO> roomHistoryCannedReports = new ArrayList<RoomHistoryCannedReportDTO>();
		RoomHistoryCannedReportDTO roomHistoryCannedReportDTO = null;

		Map<String, List<Object>> map = new LinkedHashMap<String, List<Object>>();

		String roomNumber = "";
		int i = 1;

		for (Object[] list : historyList)
		{
			if (list != null && list.length > 0)
			{

				if (map.containsKey((String) list[0]) && list.length > 1)
				{
					map.get((String) list[0]).add(list[1]);
				} else
				{
					if (map.containsKey(roomNumber) && list.length > 1)
					{
						map.get(roomNumber).add(list[1]);
					}

					List<Object> object = new ArrayList<Object>();
					object.add(list[1]);
					map.put((String) list[0], object);
					roomNumber = (String) list[0];
				}
			}
		}
		for (Map.Entry<String, List<Object>> mapValues : map.entrySet())
		{
			roomHistoryCannedReportDTO = new RoomHistoryCannedReportDTO();
			roomHistoryCannedReportDTO.setRoom(mapValues.getKey());
			List<Object> objectList = mapValues.getValue();
			if (null != objectList && mapValues.getKey() != null && !mapValues.getKey().trim().isEmpty())
			{
				roomHistoryCannedReportDTO.setDateEntered((Date) objectList.get(0));
				if (i != map.size())
					roomHistoryCannedReportDTO.setDateExit((Date) objectList.get(objectList.size() - 1));

			}
			i = i + 1;
			roomHistoryCannedReports.add(roomHistoryCannedReportDTO);
		}

		/*
		 * for (Object[] list : historyList) {
		 * 
		 * if (list != null && list.length > 0) {
		 * 
		 * if (map.containsKey((String) list[0]) && list.length > 1) { map.get((String) list[0]).add(list[1]); } else { List<Object> object = new
		 * ArrayList<Object>(); object.add(list[1]); map.put((String) list[0], object); }
		 * 
		 * if (!roomNumber.equalsIgnoreCase((String) list[0])) { if (!roomNumber.equals("")) {
		 * 
		 * roomHistoryCannedReports.add(roomHistoryCannedReportDTO); i = 0; } roomHistoryCannedReportDTO = new RoomHistoryCannedReportDTO();
		 * roomNumber = (String) list[0]; roomHistoryCannedReportDTO.setRoom(list[0] == null ? "" : list[0].toString()); if (list.length > 1) {
		 * roomHistoryCannedReportDTO.setDateEntered((Date) list[1]); } } if (roomNumber.equalsIgnoreCase((String) list[0]) && list.length > 1) { i =
		 * i + 1; roomHistoryCannedReportDTO.setDateExit((Date) list[1]); }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * if (i == 1) { roomHistoryCannedReportDTO.setDateExit(null); } roomHistoryCannedReports.add(roomHistoryCannedReportDTO);
		 */

		return roomHistoryCannedReports;
	}

	@Override
	public List<?> getAnimalDetailsInRoom(final String roomId)
	{

		Query query = entityManager.createQuery("SELECT animalId, sex,studyId FROM Animals a WHERE roomNum=?");
		query.setParameter(1, roomId);
		List<Object[]> historyList = query.getResultList();
		List<AnimalHistoryCannedReportDTO> animalHistoryCannedReports = new ArrayList<AnimalHistoryCannedReportDTO>();

		for (Object[] list : historyList)
		{
			AnimalHistoryCannedReportDTO animalHistoryCannedReportDTO = new AnimalHistoryCannedReportDTO();
			if (list != null && list.length > 0)
			{
				animalHistoryCannedReportDTO.setAnimalId(list[0] == null ? "" : list[0].toString());
				if (list.length > 1)
				{
					animalHistoryCannedReportDTO.setSex(list[1] == null ? "" : list[1].toString());
				}
				if (list.length > 2)
				{
					if (list[2] != null)
					{
						animalHistoryCannedReportDTO.setFileId(list[2] == null ? "" : list[2].toString());
					} else
					{
						animalHistoryCannedReportDTO.setFileId("");
					}
				}

			}

			animalHistoryCannedReports.add(animalHistoryCannedReportDTO);
		}

		return animalHistoryCannedReports;
	}

	@Override
	public List<?> getCbcCannedReport(final String studyId)
	{

		Query query = entityManager
				.createQuery("SELECT  at.animalId.animalId,at.collectedDate,at.wbc,at.rbc,at.hgb,at.hct,at.mcv,at.mch,at.mchc,at.nrbc,at.neutrophilSeg,"
						+ "at.neutrophilBand,at.lymphocyte,at.monocyte,at.eosinophil,at.basophil,at.metamyelocyte,"
						+ "at.myelocyte,at.promyelocyte,at.plateletEstimate,at.polychromasia,at.anisocytosis,at.poikilocytosis,at.heinzBodies,"
						+ "at.remarks,at.absoluteNeutrophilSeg,at.absoluteNeutrophilBand,at.absoluteLymphocyte,"
						+ "at.absoluteMonocyte,at.absoluteEosinophil,at.absoluteBasophil,at.absoluteMetamyelocytes,"
						+ "at.absolutePromyelocytes,at.plateletCount FROM AnimalTestResults at,Study s JOIN s.animals sa "
						+ "WHERE sa.id=at.animalId.id AND s.id=?");
		query.setParameter(1, Long.parseLong(studyId));
		List<Object[]> historyList = query.getResultList();
		List<CBCCannedReportDTO> cbcCannedReports = new ArrayList<CBCCannedReportDTO>();

		for (Object[] list : historyList)
		{
			CBCCannedReportDTO cbcCannedReportDTO = new CBCCannedReportDTO();
			if (list != null && list.length > 0)
			{

				cbcCannedReportDTO.setAnimalId(list[0] == null ? "" : list[0].toString());

				if (list.length > 1)
				{
					if (list[0] != null && list[1] instanceof Date)
					{
						cbcCannedReportDTO.setCollectedDate((Date) list[1]);
					}

				}
				if (list.length > 2)
				{
					cbcCannedReportDTO.setWbc(list[2] == null ? "" : list[2].toString());
				}
				if (list.length > 3)
				{
					cbcCannedReportDTO.setRbc(list[3] == null ? "" : list[3].toString());
				}
				if (list.length > 4)
				{
					cbcCannedReportDTO.setHgb(list[4] == null ? "" : list[4].toString());
				}
				if (list.length > 5)
				{
					cbcCannedReportDTO.setHct(list[5] == null ? "" : list[5].toString());
				}
				if (list.length > 6)
				{
					cbcCannedReportDTO.setMcv(list[6] == null ? "" : list[6].toString());
				}
				if (list.length > 7)
				{
					cbcCannedReportDTO.setMch(list[7] == null ? "" : list[7].toString());
				}
				if (list.length > 8)
				{
					cbcCannedReportDTO.setMchc(list[8] == null ? "" : list[8].toString());
				}
				if (list.length > 9)
				{
					cbcCannedReportDTO.setNrbc(list[9] == null ? "" : list[9].toString());
				}
				if (list.length > 10)
				{
					cbcCannedReportDTO.setNeutrophilSeg(list[10] == null ? "" : list[10].toString());
				}
				if (list.length > 11)
				{
					cbcCannedReportDTO.setNeutrophilBand(list[11] == null ? "" : list[11].toString());
				}
				if (list.length > 12)
				{
					cbcCannedReportDTO.setLymphocyte(list[12] == null ? "" : list[12].toString());
				}
				if (list.length > 13)
				{
					cbcCannedReportDTO.setMonocyte(list[13] == null ? "" : list[13].toString());
				}
				if (list.length > 14)
				{
					cbcCannedReportDTO.setEosinophil(list[14] == null ? "" : list[14].toString());
				}
				if (list.length > 15)
				{
					cbcCannedReportDTO.setBasophil(list[15] == null ? "" : list[15].toString());
				}
				if (list.length > 16)
				{
					cbcCannedReportDTO.setMetamyelocyte(list[16] == null ? "" : list[16].toString());
				}
				if (list.length > 17)
				{
					cbcCannedReportDTO.setMyelocyte(list[17] == null ? "" : list[17].toString());
				}
				if (list.length > 18)
				{
					cbcCannedReportDTO.setPromyelocyte(list[18] == null ? "" : list[18].toString());
				}
				if (list.length > 19)
				{
					cbcCannedReportDTO.setPlateletEstimate(list[19] == null ? "" : list[19].toString());
				}
				if (list.length > 20)
				{
					cbcCannedReportDTO.setPolychromasia(list[20] == null ? "" : list[20].toString());
				}

				if (list.length > 21)
				{
					cbcCannedReportDTO.setAnisocytosis(list[21] == null ? "" : list[21].toString());
				}
				if (list.length > 22)
				{
					cbcCannedReportDTO.setPoikilocytosis(list[22] == null ? "" : list[22].toString());
				}
				if (list.length > 23)
				{
					cbcCannedReportDTO.setHeinzBodies(list[23] == null ? "" : list[23].toString());
				}
				if (list.length > 24)
				{
					cbcCannedReportDTO.setRemarks(list[24] == null ? "" : list[24].toString());
				}

				if (list.length > 25)
				{
					cbcCannedReportDTO.setAbsoluteNeutrophilSeg(list[25] == null ? "" : list[25].toString());
				}
				if (list.length > 26)
				{
					cbcCannedReportDTO.setAbsoluteNeutrophilBand(list[26] == null ? "" : list[26].toString());
				}
				if (list.length > 27)
				{
					cbcCannedReportDTO.setAbsoluteLymphocyte(list[27] == null ? "" : list[27].toString());
				}
				if (list.length > 28)
				{
					cbcCannedReportDTO.setAbsoluteMonocyte(list[28] == null ? "" : list[28].toString());
				}
				if (list.length > 29)
				{
					cbcCannedReportDTO.setAbsoluteEosinophil(list[29] == null ? "" : list[29].toString());
				}
				if (list.length > 30)
				{
					cbcCannedReportDTO.setAbsoluteBasophil(list[30] == null ? "" : list[30].toString());
				}
				if (list.length > 31)
				{
					cbcCannedReportDTO.setAbsoluteMetamyelocytes(list[31] == null ? "" : list[31].toString());
				}
				if (list.length > 32)
				{
					cbcCannedReportDTO.setAbsolutePromyelocytes(list[32] == null ? "" : list[32].toString());
				}
				if (list.length > 33)
				{
					cbcCannedReportDTO.setPlateletCount(list[33] == null ? "" : list[33].toString());
				}
			}

			cbcCannedReports.add(cbcCannedReportDTO);
		}

		return cbcCannedReports;
	}

	@Override
	public List<?> getAnimalWeights(final String studyId)
	{
		Query query = entityManager.createQuery("SELECT a.animalId,a.weight FROM Animals a,Study s JOIN s.animals sa WHERE sa.id=a.id AND s.id=?");
		query.setParameter(1, Long.parseLong(studyId));
		List<Object[]> historyList = query.getResultList();
		List<AnimalWeightReportsDTO> animalWeightReports = new ArrayList<AnimalWeightReportsDTO>();

		for (Object[] list : historyList)
		{
			AnimalWeightReportsDTO animalWeightReportsDTO = new AnimalWeightReportsDTO();
			if (list != null && list.length > 0)
			{
				if (list[0] instanceof Long)
				{
					animalWeightReportsDTO.setAnimalId(String.valueOf((Long) list[0]));
				} else if (list[0] instanceof String)
				{
					animalWeightReportsDTO.setAnimalId(list[0] == null ? "" : list[0].toString());
				}

			}

			if (list != null && list.length > 1)
			{
				animalWeightReportsDTO.setWeights(list[1] == null ? "" : list[1].toString());
			}
			animalWeightReports.add(animalWeightReportsDTO);
		}

		return animalWeightReports;
	}

	@Override
	public List<?> getJobCodeDetails(String jobCode, Date fromDate, Date toDate)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(toDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		calendar.set(year, month, day, 23, 59, 59);
		Date date = calendar.getTime();

		Query query = entityManager
				.createQuery("SELECT animalId,addedDate FROM AnimalHistory WHERE chargeCode=? AND addedDate BETWEEN ? AND ? ORDER BY addedDate ASC");

		query.setParameter(1, jobCode);
		query.setParameter(2, fromDate);
		query.setParameter(3, date);
		List<Object[]> historyList = query.getResultList();

		Map<String, List<Date>> dateDetails = new HashMap<String, List<Date>>();

		for (Object[] list : historyList)
		{

			if (list != null && list.length > 1)
			{

				if (dateDetails.containsKey(list[0].toString()))
				{
					dateDetails.get(list[0].toString()).add((Date) list[1]);
				} else
				{
					List<Date> dateList = new ArrayList<Date>();
					dateList.add((Date) list[1]);
					dateDetails.put(list[0].toString(), dateList);
				}

			}

		}

		List<AnimalJobCodeDetailReportsDTO> animalJobCodeDetailReports = new ArrayList<AnimalJobCodeDetailReportsDTO>();

		if (!dateDetails.isEmpty())
		{
			for (Map.Entry<String, List<Date>> map : dateDetails.entrySet())
			{
				AnimalJobCodeDetailReportsDTO animalJobCodeDetailReportsDTO = new AnimalJobCodeDetailReportsDTO();
				animalJobCodeDetailReportsDTO.setAnimalId(map.getKey());
				List<Date> dateValues = map.getValue();
				if (null != dateValues)
				{
					animalJobCodeDetailReportsDTO.setStartDate(dateValues.get(0));
					animalJobCodeDetailReportsDTO.setEndDate(dateValues.get(dateValues.size() - 1));

					Calendar calendarValue = Calendar.getInstance();
					calendarValue.setTime(animalJobCodeDetailReportsDTO.getEndDate());
					int yearValue = calendarValue.get(Calendar.YEAR);
					int monthValue = calendarValue.get(Calendar.MONTH);
					int dayValue = calendarValue.get(Calendar.DATE);
					calendarValue.set(yearValue, monthValue, dayValue, 23, 59, 59);
					Date dateValue = calendarValue.getTime();

					Calendar calendarValueFrom = Calendar.getInstance();
					calendarValueFrom.setTime(animalJobCodeDetailReportsDTO.getStartDate());
					int yearValueFrom = calendarValueFrom.get(Calendar.YEAR);
					int monthValueFrom = calendarValueFrom.get(Calendar.MONTH);
					int dayValueFrom = calendarValueFrom.get(Calendar.DATE);
					calendarValueFrom.set(yearValueFrom, monthValueFrom, dayValueFrom, 0, 0, 0);
					Date dateValueFrom = calendarValueFrom.getTime();

					long days = dateValue.getTime() - dateValueFrom.getTime();

					animalJobCodeDetailReportsDTO.setDays(TimeUnit.DAYS.convert(days, TimeUnit.MILLISECONDS));
				}
				animalJobCodeDetailReports.add(animalJobCodeDetailReportsDTO);
			}
		}

		return animalJobCodeDetailReports;
	}

	/*
	 * @Override public List<?> getAnimalTreatmentDetails(String animalId) { // ORDER BY sp.procedureSchedules.scheduleDate DESC Query query =
	 * entityManager.createQuery("FROM StudyProcedure sp JOIN sp.animals a  WHERE a.animalId=?"); query.setParameter(1, animalId); List<Object[]>
	 * studyProcedure = query.getResultList(); List<AnimalTreatmentCannedReportDTO> animalTreatmentCannedReportDTOs = new
	 * ArrayList<AnimalTreatmentCannedReportDTO>(); if (null != studyProcedure && !studyProcedure.isEmpty()) { for (Object[] object : studyProcedure)
	 * { if (object[0] instanceof StudyProcedure) { StudyProcedure procedure = (StudyProcedure) object[0];
	 * 
	 * AnimalTreatmentCannedReportDTO animalTreatmentCannedReportDTO = new AnimalTreatmentCannedReportDTO(); if (null != procedure.getTreatment()) {
	 * animalTreatmentCannedReportDTO.setTreatment(procedure.getTreatment().getGenericName() == null ? "" : procedure.getTreatment()
	 * .getGenericName()); } if (null != procedure.getProcedureSchedules() && !procedure.getProcedureSchedules().isEmpty()) { if (null !=
	 * procedure.getProcedureSchedules().get(0)) {
	 * animalTreatmentCannedReportDTO.setEndDate(procedure.getProcedureSchedules().get(0).getScheduleDate()); }
	 * 
	 * if (null != procedure.getProcedureSchedules().get(procedure.getProcedureSchedules().size() - 1)) {
	 * animalTreatmentCannedReportDTO.setStartDate(procedure.getProcedureSchedules() .get(procedure.getProcedureSchedules().size() -
	 * 1).getScheduleDate()); } }
	 * 
	 * animalTreatmentCannedReportDTOs.add(animalTreatmentCannedReportDTO); } }
	 * 
	 * } return animalTreatmentCannedReportDTOs; }
	 */

	@Override
	public List<?> getAnimalTreatmentDetails(String animalId)
	{
		Query query = entityManager
				.createNativeQuery("SELECT t.generic_name,ps.schedule_date FROM animals a,study_procedure_animal spa,study_procedure_procedure_schedules spps,study_procedure sp,procedure_schedule ps,treatment t WHERE spa.animals_id=a.id AND spps.study_procedure_id =spa.study_procedure AND a.animal_id=? AND t.id=sp.treatment_id AND sp.id=spa.study_procedure AND sp.id=spps.study_procedure_id  AND ps.id=spps.procedure_schedules_id AND t.generic_name IS NOT NULL group by t.generic_name,ps.schedule_date");
		query.setParameter(1, animalId);
		List<Object[]> studyProcedure = query.getResultList();
		List<AnimalTreatmentCannedReportDTO> animalTreatmentCannedReportDTOs = new ArrayList<AnimalTreatmentCannedReportDTO>();
		AnimalTreatmentCannedReportDTO animalTreatmentCannedReportDTO = null;
		String genericName = "";
		boolean isEndDate = false;
		int i = 0;
		if (null != studyProcedure && !studyProcedure.isEmpty())
		{
			for (Object[] object : studyProcedure)
			{
				if (object.length > 0)
				{
					if (!genericName.equalsIgnoreCase((String) object[0]))
					{
						if (!genericName.equals(""))
						{
							if (i == 1)
							{
								animalTreatmentCannedReportDTO.setEndDate(null);
							}
							animalTreatmentCannedReportDTOs.add(animalTreatmentCannedReportDTO);
							i = 0;
						}
						animalTreatmentCannedReportDTO = new AnimalTreatmentCannedReportDTO();
						genericName = (String) object[0];
						animalTreatmentCannedReportDTO.setTreatment(genericName);
						if (object.length > 1)
						{
							animalTreatmentCannedReportDTO.setStartDate((Date) object[1]);
						}
					}
					if (genericName.equalsIgnoreCase((String) object[0]) && object.length > 1)
					{
						i = i + 1;
						animalTreatmentCannedReportDTO.setEndDate((Date) object[1]);
					}
				}

			}
			if (i == 1)
			{
				animalTreatmentCannedReportDTO.setEndDate(null);
			}
			animalTreatmentCannedReportDTOs.add(animalTreatmentCannedReportDTO);
		}

		return animalTreatmentCannedReportDTOs;
	}

	@Override
	public List<?> getRhesusAnimalDetails()
	{
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.YEAR, -14);
		Date date = new Date(c.getTime().getTime());

		Query query = entityManager
				.createNativeQuery("SELECT DISTINCT ah.animal_id,ah.sex,s.id,ah.weight,ah.date_of_birth,mt.result,s.protocol_id FROM animal_history ah, study s, mamu_test mt,mamu_list ml,study_animals sa where ah.sex=? AND ah.date_of_birth >= ? AND  ah.animal_table_id=mt.animals_id AND ah.animal_table_id=sa.animals_id AND s.id=sa.study_id  AND CAST(ah.weight AS UNSIGNED) > ? AND ml.id=mt.mamu_id AND ml.mamu_name=?");

		query.setParameter(1, "Female");
		query.setParameter(2, date);
		query.setParameter(3, '5');
		query.setParameter(4, "A01");
		List<Object[]> cannedMamuReport = query.getResultList();
		List<MamuCannedReportDTO> cannedReportDTOs = new ArrayList<MamuCannedReportDTO>();
		if (null != cannedMamuReport && !cannedMamuReport.isEmpty())
		{
			for (Object[] list : cannedMamuReport)
			{
				MamuCannedReportDTO mamuCannedReportDTO = new MamuCannedReportDTO();
				if (null != list && list.length > 0)
				{
					mamuCannedReportDTO.setAnimal(list[0] == null ? "" : list[0].toString());
				}

				if (list.length > 1)
				{
					mamuCannedReportDTO.setSex(list[1] == null ? "" : list[1].toString());
				}
				if (list.length > 2)
				{
					mamuCannedReportDTO.setFile(list[2] == null ? "" : list[2].toString());
				}
				if (list.length > 3)
				{
					mamuCannedReportDTO.setWeight(list[3] == null ? "" : list[3].toString());
				}
				if (list.length > 4)
				{
					mamuCannedReportDTO.setDob(list[4] == null ? null : (Date) list[4]);
				}
				if (list.length > 5)
				{
					mamuCannedReportDTO.setA01(list[5] == null ? "" : list[5].toString());
				}
				if (list.length > 6)
				{
					mamuCannedReportDTO.setProtocol(list[6] == null ? "" : list[6].toString());
				}

				cannedReportDTOs.add(mamuCannedReportDTO);
			}
		}

		return cannedReportDTOs;
	}

	public List<Map<String, Object>> studyAnimalHistory(List<ReportDTO> reportDTOs, ReportDTO animalHistory, ReportDTO study,
			List<String> responseColumns, QueryBuilder queryBuilder, List<Map<String, Object>> response, ReportDTO procedure, boolean isProcedure, String condition)
	{

		StringBuilder select = new StringBuilder("SELECT ");
		StringBuilder where = null;
		int whereSize = 0;
		if (!isProcedure)
		{
			where = new StringBuilder("WHERE st.id = sa.study_id and ah.animal_table_id=sa.animals_id ");
		} else
		{
			where = new StringBuilder(
					"WHERE sps.study_id = st.id and sps.procedures_id= spa.study_procedure = pr.id and spa.animals_id = ah.animal_table_id ");
		}

		
		whereSize = where.length();
		StringBuilder query = new StringBuilder("");
		boolean isStarting = true;
		ReportDTO reportDTO = null;
		int columnIndex = 0;

		for (ReportDTO dTO : reportDTOs)
		{
			if (dTO.equals(animalHistory))
			{
				reportDTO = dTO;
				columnIndex = buildHistoryReport(reportDTO, isStarting, select, responseColumns, columnIndex, "ah", ReportUtil.ANIMAL_HISTORY, where,condition);
				isStarting = false;
				break;
			}
		}

		for (ReportDTO dTO : reportDTOs)
		{
			if (dTO.equals(study))
			{
				reportDTO = dTO;
				columnIndex = buildHistoryReport(reportDTO, isStarting, select, responseColumns, columnIndex, "st", ReportUtil.STUDY, where,condition);
				isStarting = false;
				break;
			}
		}

		for (ReportDTO dTO : reportDTOs)
		{
			if (dTO.equals(procedure))
			{
				reportDTO = dTO;
				columnIndex = buildHistoryReport(reportDTO, isStarting, select, responseColumns, columnIndex, "pr", ReportUtil.PROCEDURE, where,condition);
				isStarting = false;
				break;
			}
		}

		String whereC = "";
		if(where.length() != whereSize)
		{
			whereC = where.insert(whereSize, " and ( ").append(")").toString().replace("(   or", "(   ").replace("(   and", "(   ");
		}
		else
		{
			whereC = where.toString();
		}
		if (!isProcedure)
		{
			query = select.append(" from animal_history ah, study st, study_animals sa ").append(whereC);
		} else
		{
			query = select.append(" from animal_history ah, study st, study_procedure pr, study_procedure_animal spa, study_procedures sps ").append(
					whereC);
		}

		List<Object> resultlist = entityManager.createNativeQuery(query.toString()).getResultList();

		for (Object object : resultlist)
		{
			LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
			if (object != null)
			{
				if (object instanceof Object[])
				{
					Object[] resultArray = (Object[]) object;

					for (String string : queryBuilder.getColumnOrder())
					{
						for (int i = 0; i < resultArray.length; i++)
						{

							if (string.equalsIgnoreCase(responseColumns.get(i)))
							{
								Object obj = resultArray[i];

								if (obj instanceof Date)
								{
									Date date = (Date) obj;
									row.put(responseColumns.get(i), new SimpleDateFormat("MM/dd/yyyy").format(date));
								} else
								{
									row.put(responseColumns.get(i), resultArray[i]);
								}

								break;
							}

						}
					}
				} else if (object instanceof String)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else if (object instanceof Long)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				}
			}
			response.add(row);
		}

		return response;
	}

	private int buildHistoryReport(ReportDTO reportDTO, boolean isStarting, StringBuilder select, List<String> responseColumns, int columnIndex,
			String alias, String entity, StringBuilder where, String condition)
	{
		for (String columns : reportDTO.getColumns())
		{
			if (isStarting)
			{
				select.append(alias + "." + columns.replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " as " + alias + "_" + columns);
				isStarting = false;
			} else
			{
				select.append("," + alias + "." + columns.replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " as " + alias + "_" + columns);
			}

			responseColumns.add(columnIndex, entity + "." + columns);
			columnIndex++;

		}

		if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
		{

			for (WhereClause whereClause : reportDTO.getWhereClause())
			{
				if (whereClause.getType().equalsIgnoreCase("EQUAL"))
				{
					where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " = '"
							+ whereClause.getValue() + "'");
				}
				if (whereClause.getType().equalsIgnoreCase("STARTSWITH"))
				{
					where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " like '"
							+ whereClause.getValue() + "%'");
				}
				if (whereClause.getType().equalsIgnoreCase("ENDSWITH"))
				{
					where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " like '%"
							+ whereClause.getValue() + "'");
				}
				if (whereClause.getType().equalsIgnoreCase("BETWEEN"))
				{
					boolean isNumber;
					boolean isDate;

					Long value1 = null;
					Long value2 = null;

					Date fromDate = null;
					Date toDate = null;

					try
					{
						value1 = Long.parseLong(whereClause.getValue());
						value2 = Long.parseLong(whereClause.getOtherValue());
						isNumber = true;
					} catch (NumberFormatException e)
					{
						isNumber = false;
					}

					try
					{
						DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
						fromDate = format.parse(whereClause.getValue());
						toDate = format.parse(whereClause.getOtherValue());
						isDate = true;
					} catch (Exception e)
					{
						isDate = false;
					}
					if (isNumber)
					{
						where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " between ("
								+ value1 + "," + value2 + ") ");
					} else if (isDate)
					{
						where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " between ('"
								+ fromDate + "','" + toDate + "') ");
					}

				}
				if (whereClause.getType().equalsIgnoreCase("GREATERTHAN"))
				{
					where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " >= '"
							+ whereClause.getValue() + "'");
				}
				if (whereClause.getType().equalsIgnoreCase("LESSTHAN"))
				{
					where.append("  "+condition + "  " + alias + "." + whereClause.getColumn().replaceAll("(.)([A-Z])", "$1_$2").toLowerCase() + " <= '"
							+ whereClause.getValue() + "'");
				}

			}
		}

		return columnIndex;
	}

	@Override
	public List<?> getReportList()
	{
		List<Constants> constantList = constantsRepository.findByName("reportid");
		List<ConstantReportDTO> constantDetails = new ArrayList<ConstantReportDTO>();

		if (null != constantList && !constantList.isEmpty())
		{
			for (Constants constant : constantList)
			{
				List<Constants> constantObject = constantsRepository.findByName("reportid." + constant.getValue() + ".name");

				if (null != constantObject && constantObject.size() > 0)
				{
					ConstantReportDTO constantReportDTO = new ConstantReportDTO();
					constantReportDTO.setId(Long.parseLong(constant.getValue()));
					constantReportDTO.setName(constantObject.get(0).getValue());
					constantReportDTO.setDescription(constantObject.get(0).getDescription());
					constantDetails.add(constantReportDTO);
				}
			}
		}

		return constantDetails;
	}
}
