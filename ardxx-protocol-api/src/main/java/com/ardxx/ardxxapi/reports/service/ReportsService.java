package com.ardxx.ardxxapi.reports.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ardxx.ardxxapi.reports.dto.QueryBuilder;

public interface ReportsService
{
	public List<Map<String, Object>> getReports(QueryBuilder queryBuilder);

	public List<?> getCannedReports(final Long id, final String searchId, final Date fromDate, final Date toDate) throws Exception;

	public List<?> getReportList();

}
