package com.ardxx.ardxxapi.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ardxx.ardxxapi.reports.dto.QueryBuilder;

public interface ReportsDAO
{
	public List<Map<String, Object>> getReports(QueryBuilder queryBuilder);

	public List<?> getRoomHistoryDetailsOfAnimal(final String animalId);

	public List<?> getAnimalDetailsInRoom(final String roomId);

	public List<?> getCbcCannedReport(final String studyId);

	public List<?> getAnimalWeights(final String studyId);

	public List<?> getJobCodeDetails(final String jobCode, final Date fromDate, final Date toDate);

	public List<?> getAnimalTreatmentDetails(final String animalId);

	public List<?> getRhesusAnimalDetails();

	public List<?> getReportList();
}
