package com.ardxx.ardxxapi.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ardxx.ardxxapi.common.ARDXXUtils;
import com.ardxx.ardxxapi.common.Messages;

/**
 * 
 * @author Raj
 *
 */
public class ARDXXErrorPayload implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Map<String, String>> errorMessages = new ArrayList<Map<String, String>>();

	public ARDXXErrorPayload(String... errorCode)
	{
		String errorMessage = null;
		Map<String, String> error = new HashMap<String, String>();
		for (String code : errorCode)
		{

			errorMessage = Messages.properties.getProperty(code + "."+ARDXXUtils.MESSAGE);
			error.put(code, errorMessage);

		}

		this.errorMessages.add(error);
	}

	public List<Map<String, String>> getErrorMessages()
	{
		return errorMessages;
	}

	public void setErrorMessages(List<Map<String, String>> errorMessages)
	{
		this.errorMessages = errorMessages;
	}

}
