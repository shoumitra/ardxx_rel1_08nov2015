angular.module('users').controller('UsersListCtrl', function ($scope, $state, userService, DTOptionsBuilder, DTColumnDefBuilder, $cookieStore, $rootScope, toaster) {
    $scope.usersList = [];
    $scope.canCreate = false;

    var userRoles = $rootScope.currentUser.role;

    $scope.canCreate = (userRoles.indexOf('LAB_ADM') > -1) || (userRoles.indexOf('SUP_ADM') > -1);

    var languageOptions = {
        "lengthMenu": '_MENU_ entries per page',
        "search": '<i class="fa fa-search"></i>',
        "paginate": {
            "previous": '<i class="fa fa-angle-left"></i>',
            "next": '<i class="fa fa-angle-right"></i>'
        }
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptions)
        .withColVis();

    $scope.getUsersList = function () {
        userService.usersList()
            .then(function (data) {
            if (data) {
                $scope.usersList = data;
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };
    $scope.getUsersList();

});