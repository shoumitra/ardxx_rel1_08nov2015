angular.module('reports').controller('ReportviewCtrl',function($scope, $state,$stateParams,$filter, toaster, reportService, DTOptionsBuilder, DTColumnDefBuilder, $cookieStore, $rootScope) {
	
	$scope.reportId = parseInt($stateParams.id);
	$scope.searchidName = 'Search';
	$scope.reportName = 'Report';

	$scope.reportInput = {};

	$scope.isSearched = false;
	$scope.searchid = '';

	var reportId = $scope.reportId;
	$scope.reportResult = [];
	if(reportId === 1){
		$scope.searchidName = 'Animal Id';
	} else if(reportId === 2){
		$scope.searchidName = 'Room Id';
	} else if(reportId === 3 || reportId === 4){
		$scope.searchidName = 'Study Id';
	} else if(reportId === 5){
		$scope.searchidName = 'Job Code';
	} else if(reportId === 6){
		$scope.searchidName = 'Animal Id';
	}

	var languageOptions = {
		"lengthMenu": '_MENU_ entries per page',
		"search": '<i class="fa fa-search"></i>',
		"paginate": {
			"previous": '<i class="fa fa-angle-left"></i>',
			"next": '<i class="fa fa-angle-right"></i>'
		}
	};

	$scope.dtOptions = DTOptionsBuilder
		.newOptions()
		.withOption('language', languageOptions)
		.withTableTools('assets/swf/copy_csv_xls_pdf.swf')
        .withTableToolsButtons([
        {
            'sExtends': 'collection',
            'sButtonText': 'Export',
            'aButtons': ['xls', 'pdf']
        }])
        .withOption('scrollX', '100%')
        .withColVis()
        .withColVisOption('aiExclude', [0]);

	$scope.reportResult = [];

	$scope.getReportslist = function(){
		reportService.getReportsList()
			.then(function (data) {
				var rep = $filter('filter')(data, {id: $scope.reportId});
				if(rep && rep.length > 0){
					$scope.reportName = rep[0].name;
				}
			},
				function (data) {
					toaster.pop('error', 'Error', data.errorMessage);
				});
	};

	$scope.getReportslist();

	$scope.getReportResult = function(){
		var from = '';
		var to = '';
		if($scope.reportInput.fromDate && $scope.reportInput.fromDate !== ''){
			from = window.moment($scope.reportInput.fromDate).format('YYYY-MM-DD');
		}

		if($scope.reportInput.toDate && $scope.reportInput.toDate !== ''){
			to = window.moment($scope.reportInput.toDate).format('YYYY-MM-DD');
		}

		if($scope.reportId === 5 && (from === '' || to === '')){
			toaster.pop('error', 'Error', 'From and To dates are mandatory for this report.');
			return;
		}
		//$scope.reportResult = [];
		reportService.getCannedResult($scope.reportId, $scope.searchid, from, to)
			.then(function (data) {
				$scope.isSearched = true;
				$scope.reportResult = data;
			},
				function (data) {
					toaster.pop('error', 'Error', data.errorMessage);
					$scope.reportResult = [];
				});
	};

	if($scope.reportId === 7){
		$scope.getReportResult();
	}

	window.scope = $scope;
});