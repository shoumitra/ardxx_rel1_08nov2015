angular.module('reports').controller('CannedlistCtrl',function($scope, $state, toaster, reportService, DTOptionsBuilder, DTColumnDefBuilder, $cookieStore, $rootScope) {

	var languageOptions = {
		"lengthMenu": '_MENU_ entries per page',
		"search": '<i class="fa fa-search"></i>',
		"paginate": {
			"previous": '<i class="fa fa-angle-left"></i>',
			"next": '<i class="fa fa-angle-right"></i>'
		}
	};

	$scope.dtOptions = DTOptionsBuilder.newOptions()
		.withDOM('lCfrtip')
		.withOption('language', languageOptions)
		.withColVis()
		.withColVisOption('aiExclude', [0]);

	$scope.cannedReports = [];

	$scope.getReportDetails = function(){
		reportService.getReportsList()
			.then(function (data) {
				$scope.cannedReports = data;
			},
				function (data) {
					toaster.pop('error', 'Error', data.errorMessage);
				});
	};

	$scope.getReportDetails();
});