angular.module('reports').controller('QuerybuilderCtrl',function($scope, $filter, reportService, toaster, taffy){
	
	var _ = window._;

	$scope.selectedTab = 'SEARCH';
	$scope.QueryResult = [];
	$scope.predicate = '';
    $scope.reverse = false;

    $scope.filterType = 'AND';
	$scope.entitiesList = [
		{
			name:'Study',
			realName:'study',
			columns: [
					{name:'id', displayName: 'Study Id'},
					{name:'serialNumber', displayName: 'Serial Number'},
					{name:'name', displayName: 'Name'},
					{name:'chargeCode', displayName: 'Charge Code'},
					{name:'createDate', displayName: 'Created Date'},
					{name:'closedDate', displayName: 'Closed Date'},
					{name:'comment', displayName: 'Comment'}
			]
		},
		{
			name:'Animals',
			realName:'animals',
			columns: [
				{name:'animalId', displayName: 'Animal Id'},
				{name:'id', displayName: 'Animal #'},
				{name:'oldId', displayName: 'Old Id'},
				{name:'species', displayName: 'Species'},
				{name:'sex', displayName: 'Sex'},
				{name:'client', displayName: 'Client'},
				{name:'dateOfBirth', displayName: 'Date Of Birth'},
				{name:'source', displayName: 'Source'},
				{name:'recievedDate', displayName: 'Recieved Date'},
				{name:'departedDate', displayName: 'Departed Date'},
				{name:'roomNum', displayName: 'Room Number'},
				{name:'weight', displayName: 'Weight'},
				{name:'status', displayName: 'Status'},
				{name:'origin', displayName: 'Origin'},
				{name:'strain', displayName: 'Strain'},
				{name:'otherSpecies', displayName: 'Other Species'},
				{name:'otherStrain', displayName: 'Other Strain'},
				{name:'comments', displayName: 'Comments'},
				{name:'chargeCode', displayName: 'Charge Code'},
				{name:'building', displayName: 'Building'},
				{name:'sedationRequired', displayName: 'Sedation Required'},
				{name:'substance', displayName: 'Substance'},
				{name:'dosage', displayName: 'Dosage'},
				{name:'temperature', displayName: 'Temperature'},
				{name:'pulse', displayName: 'Pulse'},
				{name:'respiration', displayName: 'Respiration'},
				{name:'notes', displayName: 'Notes'},
				{name:'studyId' , displayName: 'Study Id'},
				{name:'procedureId', displayName: 'Procedure Id'},
				{name:'scheduleId', displayName: 'Schedue Id'},
				{name:'volume', displayName: 'Volume'}
			]
		},
		{
			name:'Animal History',
			realName:'animalHistory',
			columns: [
				{name:'animalId', displayName: 'Animal Id'},
				{name:'id', displayName: 'Animal #'},
				{name:'oldId', displayName: 'Old Id'},
				{name:'species', displayName: 'Species'},
				{name:'sex', displayName: 'Sex'},
				{name:'client', displayName: 'Client'},
				{name:'dateOfBirth', displayName: 'Date Of Birth'},
				{name:'source', displayName: 'Source'},
				{name:'recievedDate', displayName: 'Recieved Date'},
				{name:'departedDate', displayName: 'Departed Date'},
				{name:'roomNum', displayName: 'Room Number'},
				{name:'weight', displayName: 'Weight'},
				{name:'status', displayName: 'Status'},
				{name:'origin', displayName: 'Origin'},
				{name:'strain', displayName: 'Strain'},
				{name:'otherSpecies', displayName: 'Other Species'},
				{name:'otherStrain', displayName: 'Other Strain'},
				{name:'comments', displayName: 'Comments'},
				{name:'chargeCode', displayName: 'Charge Code'},
				{name:'building', displayName: 'Building'},
				{name:'sedationRequired', displayName: 'Sedation Required'},
				{name:'substance', displayName: 'Substance'},
				{name:'dosage', displayName: 'Dosage'},
				{name:'temperature', displayName: 'Temperature'},
				{name:'pulse', displayName: 'Pulse'},
				{name:'respiration', displayName: 'Respiration'},
				{name:'notes', displayName: 'Notes'},
				{name:'studyId' , displayName: 'Study Id'},
				{name:'procedureId', displayName: 'Procedure Id'},
				{name:'scheduleId', displayName: 'Schedue Id'},
				{name:'volume', displayName: 'Volume'}
			]
		},
		{
			name:'Procedure',
			realName:'procedure',
			columns: [
				{name:'id', displayName: 'Procedure Id'},
				{name:'procedureType', displayName: 'Procedure Type'},
				{name:'status', displayName: 'Status'}
			]
		},
		{
			name:'Schedule',
			realName:'schedule',
			columns: [
				{name:'id', displayName: 'Schedule Id'},
				{name:'scheduleDate', displayName: 'Schedule Date'},
				{name:'status', displayName: 'Status'}
			]
		}
	];

	$scope.filterQuery = [];

	$scope.filterItems = ['equal to','starts with', 'ends with', 'between', 'less than', 'more than'];
	$scope.selectedEntities = [];
	$scope.selectedColumns = [];

	$scope.resetSearch = function(){
		$scope.filterQuery = [];
		$scope.selectedEntities = [];
		$scope.selectedColumns = [];
		var selectedEntities = $filter('filter')($scope.selectedEntities,{selected:true});
		var whereType = '';
		$scope.columnOrder = [];
		angular.forEach(selectedEntities, function(entity, index){
			entity.selected = false;
		});
	};

	$scope.setFilterType = function(typeOfFilter){
		$scope.filterType = typeOfFilter;
	};

	$scope.addEntities = function(entity){
		entity.selected = true;
		$scope.selectedEntities.push(entity);
	};

	$scope.deleteEntity = function(entity, index){
		entity.selected = false;
  		$scope.selectedEntities.splice(index, 1);
	};

	$scope.addSelectedColumns = function (colName, entityName, realName, displayName, column){
		column.selected = true;
		$scope.selectedColumns.push({'sl':$scope.selectedColumns.length, 'column':colName, 'entity':entityName,'entityRealname':realName,'displayName':displayName, 'actualColumn':realName + '.' + colName});
		
	};

	$scope.deleteSelectedColumn = function(item){
		var selectedEntity = $filter('filter')($scope.entitiesList, {'realName':item.entityRealname}, true);
		if(selectedEntity.length>0){
			var selectedColumn = $filter('filter')(selectedEntity[0].columns, {'name':item.column}, true);
			if(selectedColumn.length > 0){
				selectedColumn[0].selected = false;
			}
		}
		var index = $scope.selectedColumns.indexOf(item);
  		$scope.selectedColumns.splice(index, 1);
	};

	$scope.selectedFilters = [];

	$scope.addSelectedFilters = function (colName, entityName, type, displayName){
		$scope.selectedFilters.push({'sl':$scope.selectedColumns.length, 'column':colName,'colType':type, 'entity':entityName, 'filterType':'equal to','displayName':displayName});
	};

	$scope.deleteSelectedFilter = function(item){
		var index = $scope.selectedFilters.indexOf(item);
  		$scope.selectedFilters.splice(index, 1);
	};

	var dbQuery = taffy();

	$scope.queryResults = function(){

		$scope.predicate = '';
    	$scope.reverse = false;

		$scope.filterQuery = [];

		var selectedEntities = $filter('filter')($scope.selectedEntities,{selected:true});
		var whereType = '';
		$scope.columnOrder = [];
		angular.forEach(selectedEntities, function(entity, index){
			var table = {'table':angular.lowercase(entity.realName),columns : [], whereClause : []};
			
			var selectedCols = [];
			selectedCols =  $filter('filter')($scope.selectedColumns,{entity:entity.name},true);
			angular.forEach(selectedCols, function(col, index){
				table.columns.push(col.column);
				$scope.columnOrder.push(col.entityRealname+'.'+col.column);
			});

			var selectedWhere = [];
			selectedWhere =  $filter('filter')($scope.selectedFilters,{entity:entity.name},true);
			angular.forEach(selectedWhere, function(col, index){
				switch (col.filterType){
					case 'equal to':
						whereType = 'EQUAL';
						break;
					case 'starts with':
						whereType = 'STARTSWITH';
						break;
					case 'ends with':
						whereType = 'ENDSWITH';
						break;
					case 'between':
						whereType = 'BETWEEN';
						break;
					case 'less than':
						whereType = 'LESSTHAN';
						break;
					case 'more than':
						whereType = 'GREATERTHAN';
						break;
					default:
						whereType = 'EQUAL';
						break;
				}
				table.whereClause.push({column : col.column, value: col.filterText, otherValue: col.filterTextTo, type:whereType});
			});

			$scope.filterQuery.push(table);
		});

		$scope.resultColumns = angular.copy($scope.selectedColumns);

		reportService.searchReport({entities: $scope.filterQuery, columnOrder: $scope.columnOrder, condition: angular.lowercase($scope.filterType)})
           .then(function (data) {
               $scope.QueryResultOriginal = data;
               if(data.length > 0){
               	var getColumns = _.map(scope.QueryResultOriginal[0], function(num, key){ return key; });
               	$scope.resultColumns = [];
               	$scope.headerValues = [];
               	angular.forEach(scope.QueryResultOriginal[0], function(val, key){
               		$scope.resultColumns.push({colName:key});
               		$scope.headerValues.push(key);
               	})
               }
               $scope.selectedTab = 'RESULTS';

               $scope.QueryResult = angular.copy($scope.QueryResultOriginal);

               //$scope.filteredResults = _.map(scope.QueryResult, function(val, key){ return _.map(val, function(obj){ return obj; }); });
               dbQuery = taffy($scope.QueryResult);
               if(data.length > 0){
               	$scope.filterColumns = _.keys($scope.QueryResult[0]);
               }
           },
               function (data) {
                   toaster.pop('error', 'Error', data.errorMessage);
               });
	};

	$scope.filterQueryResult = function(filterAction){
		var searchResult = angular.copy($scope.QueryResultOriginal);
		$scope.filterParams = [];
		$scope.myFilter = {};
		angular.forEach($scope.resultColumns, function(val, index){
			if(angular.isDefined(val.filterType) && val.filterType !== '' && angular.isDefined(val.filterValue) && val.filterValue !== ''){
				var obj = {};
				switch (val.filterType){
					case 'equal to':
						obj[val.colName] = {'==' : val.filterValue};
						$scope.myFilter[val.colName] = {'==' : val.filterValue};
						$scope.filterParams.push(obj);
						break;
					case 'starts with':
						obj[val.colName] = {leftnocase:val.filterValue};
						$scope.myFilter[val.colName] = {leftnocase:val.filterValue};
						$scope.filterParams.push(obj);
						break;
					case 'ends with':
						obj[val.colName] = {rightnocase:val.filterValue};
						$scope.myFilter[val.colName] = {rightnocase:val.filterValue};
						$scope.filterParams.push(obj);
						break;
					case 'between':
						break;
					case 'less than':
						obj[val.colName] = {lt:val.filterValue};
						$scope.myFilter[val.colName] = {lt:val.filterValue};
						$scope.filterParams.push(obj);
						break;
					case 'more than':
						obj[val.colName] = {gt:val.filterValue};
						$scope.myFilter[val.colName] = {gt:val.filterValue};
						$scope.filterParams.push(obj);
						break;
					default:
						break;
				}
			} else if(val.filterType === '' && filterAction === 'type'){
				val.filterValue = '';
			}
		});
		if($scope.filterParams.length > 0)
		{
			var orderText = $scope.predicate + ' ' + ($scope.reverse ? 'desc' : 'asec');
			$scope.QueryResult = dbQuery($scope.myFilter).order(orderText).get();
		}
		//if($scope.filterParams.length > 0){
		//	$scope.filteredResults = _.map(dbQuery($scope.myFilter).get(), function(val, key){ return _.map(val, function(obj){ return obj; }); });
		//}
		//else{
		//	$scope.filteredResults = _.map($scope.QueryResult, function(val, key){ return _.map(val, function(obj){ return obj; }); });
		//}
		
	};

	$scope.deleteResultValue = function(index){
		$scope.QueryResult.splice(index,1);
		$scope.filterQueryResult();
	};

	$scope.goToPivot = function(){
		$scope.selectedTab = 'PIVOT';

		$("#pivotOutput").pivotUI($scope.QueryResult,{hiddenAttributes:['$$hashKey','___id', '___s']}, true);
	};

	// Simple type mapping; dates can be hard
	// and I would prefer to simply use `datevalue`
	// ... you could even add the formula in here.
	testTypes = {
	    "name": "String",
	    "city": "String",
	    "country": "String",
	    "birthdate": "String",
	    "amount": "Number"
	};

	emitXmlHeader = function () {
	    var headerRow =  '<ss:Row>\n';
	    angular.forEach ($scope.headerValues,function (colName) {
	        headerRow += '  <ss:Cell>\n';
	        headerRow += '    <ss:Data ss:Type="String">';
	        headerRow += colName + '</ss:Data>\n';
	        headerRow += '  </ss:Cell>\n';        
	    })
	    headerRow += '</ss:Row>\n';    
	    return '<?xml version="1.0"?>\n' +
	           '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">\n' +
	           '<ss:Worksheet ss:Name="Sheet1">\n' +
	           '<ss:Table>\n\n' + headerRow;
		};

		emitXmlFooter = function() {
		    return '\n</ss:Table>\n' +
		           '</ss:Worksheet>\n' +
		           '</ss:Workbook>\n';
		};

		jsonToSsXml = function (jsonObject) {
		    var row;
		    var col;
		    var xml;
		    var data = typeof jsonObject != "object" 
		             ? JSON.parse(jsonObject) 
		             : jsonObject;

		    xml = emitXmlHeader();

		    for (row = 0; row < data.length; row++) {
		        xml += '<ss:Row>\n';

		        angular.forEach(data[row], function(val, key){
		        	if(key !== '$$hashKey' && key !== '___id' && key !== '___s'){
		        		xml += '  <ss:Cell>\n';
			            xml += '    <ss:Data ss:Type="' + 'String'  + '">';
			            xml += val + '</ss:Data>\n';
			            xml += '  </ss:Cell>\n';
		        	}
		        })

		        xml += '</ss:Row>\n';
		    }

		    xml += emitXmlFooter();
		    return xml;  
		};

	$scope.getQueryArray = function(){
		var dataArray = angular.copy($scope.QueryResult);
		for (row = 0; row < dataArray.length; row++) {
		        delete dataArray[row]['___id'];
		        delete dataArray[row]['___s'];
		    }
		return dataArray;
	}

	$scope.exportToExcel = function(){
		
		

		var blob = new Blob([jsonToSsXml($scope.QueryResult)], {
	        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	    });
	    saveAs(blob, "QueryResult.xls");
	
	};

	$scope.getExportHeaders = function(){
		var headers = [];
		angular.forEach($scope.selectedColumns, function(col, index){
			headers.push(col.displayName);
		});
		return headers;
	};

	$scope.orderData = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
        var orderText = $scope.predicate + ' ' + ($scope.reverse ? 'logicaldesc' : 'logical');
        $scope.QueryResult = dbQuery().order(orderText).get();
      };

	window.scope = $scope;
});