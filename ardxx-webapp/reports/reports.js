angular.module('reports', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('reports').config(function($stateProvider) {

    $stateProvider.state('querybuilder', {
        url: '/report-query',
        templateUrl: 'reports/partial/querybuilder/querybuilder.html',
        data: { pageTitle: 'Query Builder' }
    });
    $stateProvider.state('cannedlist', {
        url: '/cannedlist',
        templateUrl: 'reports/partial/cannedlist/cannedlist.html',
        data: { pageTitle: 'Canned Reports' }
    });
    $stateProvider.state('reportView', {
        url: '/reportview/:id',
        templateUrl: 'reports/partial/reportView/reportView.html',
        data: { pageTitle: 'Report' }
    });
    /* Add New States Above */

});

