angular.module('reports').factory('reportService',function($q, $http, configService) {

	var reportService = {};

	reportService.searchReport = function (conditions) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'reports',conditions)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    reportService.getReportsList = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'reports/list')
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    reportService.getCannedResult = function (reportId,searchid,fromdate,todate) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'reports/canned?reportid=' + reportId + '&searchid='+ searchid + '&fromdate=' + fromdate +'&todate=' + todate)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

	return reportService;
});