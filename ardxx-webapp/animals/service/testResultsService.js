angular.module('animals').factory('testResultsService', function ($http, configService, $q) {

	var testResultsService = {};

	testResultsService.getAllTestResults = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/animalTestsResults')
            .success(function (data) {
            deferred.resolve(data);
        })
            .error(function () {
            deferred.reject('Failed to get animals test results.');
        });
        return deferred.promise;
    };

	testResultsService.getTestResultsById = function (id) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/animalTestsResults/' + id)
            .success(function (data) {
            deferred.resolve(data);
        })
            .error(function () {
            deferred.reject('Failed to get animals test results.');
        });
        return deferred.promise;
    };
	
	testResultsService.getTestResultsforAnimal = function (animalId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/animalTestsResults/animals/' + animalId)
            .success(function (data) {
            deferred.resolve(data);
        })
            .error(function () {
            deferred.reject('Failed to get animals test results.');
        });
        return deferred.promise;
    };

	return testResultsService;
});