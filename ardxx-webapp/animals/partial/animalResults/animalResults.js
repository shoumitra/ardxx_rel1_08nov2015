; (function (c3) {  // jshint ignore:line
    'use strict';
    angular.module('animals').controller('AnimalresultsCtrl', function ($scope) {
        $scope.chart = null;

        $scope.chartType = 'spline';

        $scope.timeLines = ['2015-01-01', '2015-01-08', '2015-01-15', '2015-01-22', '2015-01-29', '2015-02-06', '2015-02-13', '2015-02-20'];
        $scope.rbcCount = [6.5, 7.5, 9, 8.3, 9.6, 5.2, 6.4, 8.2];
        $scope.wbcCount = [10, 9.8, 12.2, 15.6, 17.7, 12.4, 14.4, 16];
        $scope.showGraph = function () {
            $scope.chart = c3.generate({
                data: {
                    x: 'x',
                    json: {
                        'x': ['2015-01-01', '2015-01-08', '2015-01-15', '2015-01-22', '2015-01-29', '2015-02-06', '2015-02-13', '2015-02-20'],
                        'RBC': [6.5, 7.5, 9, 8.3, 9.6, 5.2, 6.4, 8.2],
                        'WBC': [10, 9.8, 12.2, 15.6, 17.7, 12.4, 14.4, 16]
                    },
                    type: $scope.chartType
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: '%Y-%m-%d'
                        }
                    }
                },
                color: {
                    pattern: ['#1f77b4', '#ff7f0e']
                },
                grid: {
                    y: {
                        lines: [
                            { value: 8, text: 'Minimum count (8)' },
                            { value: 15, text: 'Maximum count (15)'}
                        ]
                    }
                }
            });

            $scope.$watch('chartType', function () {
                $scope.chart.transform($scope.chartType);
            });
        };

        $scope.showGraph();


    });
/* jshint ignore:start */
}(c3));
/* jshint ignore:end */