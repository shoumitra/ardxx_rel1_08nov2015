<a name"0.4.13"></a>
### 0.4.13 (2015-07-21)


#### Bug Fixes

* AR-140 - Error messages displaying in seperate lines (6cef7fcd)
* AR-132 - Elisa virus list issue (e1702ee8)
* changed species other condition (6aa51b7f)
* added strain and comments (91800739)
* AR-139: animal species list sync with BE (080f3f2f)
* AR-135 - Approval dates auto refresh (e1153462)
* AR-125 - Origin made as country and continent list (5e76f3bb)
* AR-129 - External user creation issue (8b920ede)
* AR-71 - noo committee member selection on full committee review (b55e9685)
* AR-101 - role edit option for admins for their own profiles (e04ad773)
* added default page number for protocol list (63f830ae)
* AR-108 - Animal id removed from create and edit, old id made optional (e4e27816)
* code corrected (6d54a3e4)
* password pattern and phone number format change (ae15d980)
* pdf null value handling (9f07857e)
* AR-90 - Telephone, fax and Email format validation (3ed89ff6)


#### Features

* Study management initial working prototype (e44e1683)


### [0.4.12](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.12) (2015-07-06)


#### Bug Fixes

* AR-99 - disable confirm password when password is not entered (28b0b783)
* AR-97 - Breadcrumb change (89f0d0d2)
* AR-96 - email ID, First name, last name editable by only ADMIN (1a8f239e)
* AR-95 - role and password is mandatory, put asterisk like mandatory field (fc98b96c)
* AR-94 - Added Phone number and Status in user listing (ddf53f3d)
* AR-83 - N in Name should be capitalized (727a3b8f)
* AR-82 - changed username to Email (6862d6a8)
* AR-84 - validations implemented in User profile page (e584a7d9)
* AR-87 - Password strength validation (9f63c24c)
* PDF generation changes and Rationale section update issue fix (9174b3a1)
* uesr password update change (40b1d18a)
* user profile update issue (589ae98f)
* change in grunt file and added origin in animal details and removed virus (4e519646)
* pdf generation variable conflict issue (87eb227c)


#### Features

* Protocol PDF till J section, env display in build (7c841aac)


### [0.4.9](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.9) (2015-06-12)


#### Bug Fixes

* changed prootocol status from PREREVIEW TO PRE_REVIEW (1d837743)
* AR-59: External user flag for user with COM_MEM role (6d146115)
* AR-60: user phone number added (5e111e57)
* AR-65: Animal TODO items (339c3c13)
* my profile link issue fixe (5e38ae1b)
* user roles display issue in readonly mode (ff223ae4)
* readonly view fix on new object creation (7ea84421)
* animal status label display issue (c35566de)
* reports export issue fix (1852c3a7)
* reports export issue fix (44cd9425)


#### Features

* Default readonly view for user and animal details (2e81431f)
* grunt task for replacing api url for dev and production (d1fe874e)


### [0.4.8](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.8) (2015-06-04)


#### Bug Fixes

* AR-58 - protocol management changes (87abf8c3)
* AR-56 - session timeout in Angular app (8d53e92d)
* Role display change (9561da5b)
* ELISA and MAMU changes (8e90e359)


#### Features

* login screen design change and comments changes (6b918e3a)
* tooltip display for animal weight (10be6c5a)


### [0.4.7](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.12) (2015-05-31)


#### Bug Fixes

* animal management changes (e317346b)
* disable bump push by default (c11e3d06)


#### Features

* ELISA and MANU results manual entry (d79e883f)
* animal CBC results report (7f3bb048)
* new reports (1f060486)


### [0.4.6](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.6) (2015-05-27)


### [0.4.5](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.5) (2015-05-27)


#### Bug Fixes

* disable bump push by default (c11e3d06)


#### Features

* animal CBC results report (7f3bb048)
* new reports (1f060486)


### [0.4.3](https://bitbucket.org/thoughtnotchteam/ardxx-webapp/commits/tag/v0.4.3) (2015-05-26)


#### Bug Fixes

* disable bump push by default (c11e3d06)

