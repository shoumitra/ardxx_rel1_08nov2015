angular.module('clinicalProcedures').controller('ProcedureCreateCtrl', function($scope, $filter,$state,  toaster, studyService, protocolService) {
    $scope.procedureConstantValues = [];
    $scope.vetUsers = [];

    $scope.procedureConstantValues = function() {
        studyService.getProcedureConstantValues()
            .then(function(data) {
                $scope.procedureConstantValues = data;
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    protocolService.getUsersByRoleId(9)
        .then(function(data) {
            $scope.vetUsers = data;
        }, function(data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });

    $scope.procedureConstantValues();

    $scope.clinicalProcedure = {};
    $scope.clinicalProcedure.treatment = {};
    $scope.routeValues = [];
    $scope.frequencyValues = [];

    $scope.getConstantValue = function() {
        if ($scope.clinicalProcedure.procedureType === 'TREATMENT' && $scope.clinicalProcedure.treatment.genericName && $scope.clinicalProcedure.treatment.genericName !== '') {
            var concentrationString = 'procedure.treatment.genericname.{0}.concentration'.format($scope.clinicalProcedure.treatment.genericName);
            var concentrationUnitString = 'procedure.treatment.genericname.{0}.concentration.unit'.format($scope.clinicalProcedure.treatment.genericName);
            var dosageString = 'procedure.treatment.genericname.{0}.dosage'.format($scope.clinicalProcedure.treatment.genericName);
            var dosageUnitString = 'procedure.treatment.genericname.{0}.dosage.unit'.format($scope.clinicalProcedure.treatment.genericName);
            var routeString = 'procedure.treatment.genericname.{0}.route'.format($scope.clinicalProcedure.treatment.genericName);
            var frequencyString = 'procedure.treatment.genericname.{0}.frequency'.format($scope.clinicalProcedure.treatment.genericName);

            var constantValues = [];

            //Get concentration
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: concentrationString
            }, true);

            console.log(constantValues);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.concentration = constantValues[0].value;
            }

            //Get concentration Unit
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: concentrationUnitString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.concentrationUnit = constantValues[0].value;
            }

            //Get dosage
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: dosageString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.dosage = constantValues[0].value;
            }

            //Get dosage Unit
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: dosageUnitString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.dosageUnit = constantValues[0].value;
            }

            //Get route values
            $scope.routeValues = $filter('filter')($scope.procedureConstantValues, {
                name: routeString
            }, true);

            //Get route values
            $scope.frequencyValues = $filter('filter')($scope.procedureConstantValues, {
                name: frequencyString
            }, true);
        } else {
            $scope.clinicalProcedure.treatment.concentration = null;
            $scope.clinicalProcedure.treatment.concentrationUnit = null;
            $scope.clinicalProcedure.treatment.dosage = null;
            $scope.clinicalProcedure.treatment.dosageUnit = null;
            $scope.clinicalProcedure.treatment.routeValues = [];
            $scope.clinicalProcedure.treatment.frequencyValues = [];
        }
    };

    $scope.saveProcedureInformation = function() {

        if ($scope.clinicalProcedure.procVetTechnician) {
            $scope.clinicalProcedure.vetTechnicianId = $scope.clinicalProcedure.procVetTechnician;
        }
        studyService.addProcedure(100, $scope.clinicalProcedure)
            .then(function(data) {

                toaster.pop('success', 'Success', 'Procedure created successfully.');

                $state.go('procedure-details', { 'id': data.id});
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.assignTecToProcedure = function(userType, userId) {
        studyService.assignTecToProcedure(100, $scope.clinicalProcedure.id, userType, userId)
            .then(function(data) {
                $scope.clinicalProcedure = data;
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    window.scope = $scope;
});