angular.module('clinicalProcedures').controller('ProcedureListCtrl', function($scope, $rootScope, studyService, DTOptionsBuilder, toaster, uiCalendarConfig, $compile, $timeout) {
    var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole: 'LAB_VET',
        researchRole: 'RES_SCI',
        vetTech: 'VET_TEC',
        prjManager: 'PJT_MGR',
        vetDoc: 'VET_DOC'
    };

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;
    $scope.isVetTech = $rootScope.currentUser.role.indexOf(roles.vetTech) > -1;
    $scope.isPM = $rootScope.currentUser.role.indexOf(roles.prjManager) > -1;
    $scope.isVetDoc = $rootScope.currentUser.role.indexOf(roles.vetDoc) > -1;

    var languageOptions = {
        "lengthMenu": '_MENU_ entries per page',
        "search": '<i class="fa fa-search"></i>',
        "paginate": {
            "previous": '<i class="fa fa-angle-left"></i>',
            "next": '<i class="fa fa-angle-right"></i>'
        }
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptions)
        .withColVis()
        .withColVisOption('aiExclude', []);

    $scope.proceduresList = [];

    studyService.clinicalProceduresList()
        .then(function(data) {
            if (data.status && data.status === 404) {

            } else {
                $scope.proceduresList = data.content;
            }
        }, function(data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });

    $scope.uiConfig = {
        studyCalendar: {
            height: '100%',
            editable: ($scope.isPI || $scope.isRes || $scope.isPM),
            header: false,
            selectable: ($scope.isPI || $scope.isRes || $scope.isPM),
            dayClick: function(date, event, view) {
                var dayEvents = uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar( 'clientEvents', function(event){
                    return event.start.utc().format('MM/DD/YYYY') === date.utc().format('MM/DD/YYYY');
                });
                if(dayEvents && dayEvents.length > 0){
                    $scope.selectedScheduleDay = date.utc().format('MM/DD/YYYY');
                    $('#studyScheduleModal').modal('show');
                } else{
                    toaster.pop('error', 'Error', 'No schedules available to clone for this day.');
                }                
            }
        }
    };

    $scope.displayScheduleDate = function() {
        var selectedDate, view, format;
        if (uiCalendarConfig.calendars['studyScheduleCalendar']) {
            selectedDate = uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('getDate');
            view = uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('getView');
        } else {
            selectedDate = new Date();
        }
        if (view && view.title) {
            $scope.selectedScheduleDay = view.title;
        } else {
            $scope.selectedScheduleDay = moment(selectedDate).format("MMMM YYYY");
        }
    };

    $scope.dayChanged = function(event, element) {
        console.log(element);
    };

    $scope.changeView = function(view, calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        $scope.displayScheduleDate();
    };

    $scope.nextDay = function(calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('next');
        $scope.displayScheduleDate();
    };

    $scope.prevDay = function(calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('prev');
        $scope.displayScheduleDate();
    };

    $scope.showScheduleListView = function() {
        $scope.scheduleCalendarView = false;
    };

    $scope.showScheduleCalendarView = function() {
        $scope.scheduleCalendarView = true;
    };

    $scope.convertUtcDate = function(date, format) {
        return moment.utc(date).format(format);
    };

    $scope.studySchedules = [];

    function mapStudySchedules() {
        $scope.studySchedules = [];

        angular.forEach($scope.studySchedulesList, function (data, index) {
            var eventData = {
                id:data.id,
                title: data.procedureId + ' - ' + data.procedureType+ ' - ' + data.status+ ' - ' + data.genericName,
                start: moment.utc(data.scheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
                status: data.status
            };
            $scope.studySchedules.push(eventData);

            uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('renderEvent', eventData, true);
        });
    }

    $scope.getSchedulesByStudy = function(scheduleId) {
        studyService.getSchedulesByStudy(scheduleId)
            .then(function(data) {
                $scope.studySchedulesList = data;
                mapStudySchedules();
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.isCalenderRendered = false;

    $scope.renderStudyCalendar = function() {
        if (!$scope.isCalenderRendered) {
            $timeout(function() {
                $scope.displayScheduleDate();
                uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('render');
                uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('rerenderEvents');
                $scope.getSchedulesByStudy(100);
                $scope.isCalenderRendered = true;
            }, 0);
        }
    };

    $scope.dateToClone = '';

    $scope.cloneStudySchedules = function(){
        studyService.cloneSchedules(100,moment($scope.selectedScheduleDay).format("YYYY-MM-DDT00:00:00"),moment($scope.dateToClone).format("YYYY-MM-DDT00:00:00"))
        .then(function (data) {
            $scope.studySchedulesList = data;
            mapStudySchedules();
            toaster.pop('success', 'Success', 'Cloned sccessfully');
            $scope.selectedScheduleDay = '';
            $('#studyScheduleModal').modal('hide');
            $scope.dateToClone = '';
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

});