angular.module('clinicalProcedures').controller('ProcedureDetailsCtrl',function($scope,$rootScope, $stateParams, $filter,uiCalendarConfig, $compile, $timeout, toaster, studyService, protocolService, animalsService){
     var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole: 'LAB_TEC',
        researchRole: 'RES_SCI',
        vetTech: 'VET_TEC',
        prjManager: 'PJT_MGR',
        vetDoc: 'VET_DOC'
    };

    $scope.procVetTechnician = '';
    $scope.procLabTechnician = '';

    $scope.scheduleCalendarView = true;

    $scope.procAnimals = true;
    $scope.scheduleCalendarView = true;

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;
    $scope.isVetTech = $rootScope.currentUser.role.indexOf(roles.vetTech) > -1;
    $scope.isPM = $rootScope.currentUser.role.indexOf(roles.prjManager) > -1;
    $scope.isVetDoc = $rootScope.currentUser.role.indexOf(roles.vetDoc) > -1;

	$scope.procedureId = parseInt($stateParams.id) || 0;
	$scope.procedureConstantValues = [];
    $scope.vetUsers = [];

    $scope.unassignedAnimalsSafe = [];

    $scope.uiConfig = {
        calendar: {
            height: '100%',
            editable: ($scope.isVetDoc),
            header: false,
            selectable: ($scope.isVetDoc),
            eventRender: $scope.eventRender,
            select: function (startDate, endDate, arg, view) {
                if(view.name === 'month'){
                    toaster.pop('error', 'Error', 'Please go to week or day view to schedule this procedure.');
                } else if (window.confirm('Do you want create this schedule')) {
                    if($scope.createSchedule(startDate)){
                        var eventData;
                        eventData = {
                            title: 'Schedule',
                            start: startDate
                        };
                        uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('renderEvent', eventData, true); // stick? = true
                        uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('unselect');
                    }
                }
            },
            eventDrop: function(event, delta, revertFunc, jsEvent, ui, view){
               var schedule = {
                    'id': event.id,
                    'scheduleDate': moment(event.start).format("YYYY-MM-DDTHH:mm:ss"),
                    'status': event.status
                };
                studyService.updateSchedule(100, $scope.clinicalProcedure.id, schedule)
                .then(function (data) {
                    $scope.studyProcedure = data;
                }, function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
            }
        }
    };

    $scope.procedureConstantValues = function() {
        studyService.getProcedureConstantValues()
            .then(function(data) {
                $scope.procedureConstantValues = data;
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    protocolService.getUsersByRoleId(9)
        .then(function(data) {
            $scope.vetUsers = data;
        }, function(data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });

    $scope.procedureConstantValues();

    $scope.clinicalProcedure = {};
    $scope.clinicalProcedure.treatment = {};
    $scope.routeValues = [];
    $scope.frequencyValues = [];
    $scope.procedureAssignedAnimals = [];

    $scope.getConstantValue = function() {
        if ($scope.clinicalProcedure.procedureType === 'TREATMENT' && $scope.clinicalProcedure.treatment.genericName && $scope.clinicalProcedure.treatment.genericName !== '') {
            var concentrationString = 'procedure.treatment.genericname.{0}.concentration'.format($scope.clinicalProcedure.treatment.genericName);
            var concentrationUnitString = 'procedure.treatment.genericname.{0}.concentration.unit'.format($scope.clinicalProcedure.treatment.genericName);
            var dosageString = 'procedure.treatment.genericname.{0}.dosage'.format($scope.clinicalProcedure.treatment.genericName);
            var dosageUnitString = 'procedure.treatment.genericname.{0}.dosage.unit'.format($scope.clinicalProcedure.treatment.genericName);
            var routeString = 'procedure.treatment.genericname.{0}.route'.format($scope.clinicalProcedure.treatment.genericName);
            var frequencyString = 'procedure.treatment.genericname.{0}.frequency'.format($scope.clinicalProcedure.treatment.genericName);

            var constantValues = [];

            //Get concentration
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: concentrationString
            }, true);

            console.log(constantValues);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.concentration = constantValues[0].value;
            }

            //Get concentration Unit
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: concentrationUnitString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.concentrationUnit = constantValues[0].value;
            }

            //Get dosage
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: dosageString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.dosage = constantValues[0].value;
            }

            //Get dosage Unit
            constantValues = $filter('filter')($scope.procedureConstantValues, {
                name: dosageUnitString
            }, true);

            if (constantValues.length > 0) {
                $scope.clinicalProcedure.treatment.dosageUnit = constantValues[0].value;
            }

            //Get route values
            $scope.routeValues = $filter('filter')($scope.procedureConstantValues, {
                name: routeString
            }, true);

            //Get route values
            $scope.frequencyValues = $filter('filter')($scope.procedureConstantValues, {
                name: frequencyString
            }, true);
        } else {
            $scope.clinicalProcedure.treatment.concentration = null;
            $scope.clinicalProcedure.treatment.concentrationUnit = null;
            $scope.clinicalProcedure.treatment.dosage = null;
            $scope.clinicalProcedure.treatment.dosageUnit = null;
            $scope.clinicalProcedure.treatment.routeValues = [];
            $scope.clinicalProcedure.treatment.frequencyValues = [];
        }
    };

    $scope.changeConstantValues = function(changeType){
        $scope.getConstantValue();
        if(changeType !== 'route'){
            $scope.clinicalProcedure.treatment.route = null;
        }
        $scope.clinicalProcedure.treatment.frequency = null;
    };

    $scope.getProcedureDetails = function (procedureId) {
        studyService.getProcedureDetails(procedureId)
       .then(function (data) {
           $scope.clinicalProcedure = data;
           if ($scope.clinicalProcedure.vetTechnician) {
               $scope.clinicalProcedure.procLabTechnician = $scope.clinicalProcedure.labTechnician ? $scope.clinicalProcedure.labTechnician.id : undefined;
               $scope.clinicalProcedure.procVetTechnician = $scope.clinicalProcedure.vetTechnician ? $scope.clinicalProcedure.vetTechnician.id : undefined;
               $scope.procTechnicianDetails = $scope.clinicalProcedure.vetTechnician;
           }

           $scope.getConstantValue();
       }, function (data) {
           toaster.pop('error', 'Error', data.errorMessage);
       });
    };

    $scope.getProcedureDetails($scope.procedureId);

    $scope.saveProcedureInformation = function() {
        if ($scope.clinicalProcedure.procVetTechnician) {
            $scope.clinicalProcedure.vetTechnicianId = $scope.clinicalProcedure.procVetTechnician;
        }

        var procDetails = _.pick($scope.clinicalProcedure, 'id', 'vetTechnicianId', 'treatment', 'procedureType', 'status');
        studyService.updateProcedure(100, procDetails.id, procDetails)
            .then(function(data) {
                $scope.clinicalProcedure = data;
                $scope.detailsEditable = false;
                toaster.pop('success', 'Success', 'Procedure updated successfully.');
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
                $scope.detailsEditable = true;
            });
    };

    $scope.assignTecToProcedure = function(userType, userId) {
        studyService.assignTecToProcedure(100, $scope.clinicalProcedure.id, userType, userId)
            .then(function(data) {
                $scope.clinicalProcedure = data;
            }, function(data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.getUnassignedAnimals = function () {
        animalsService.getAnimalListing()
        .then(function (data) {
            $scope.unassignedAnimals = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.showUnassignedAnimals = function(){
        $scope.procAnimals = false;

        $scope.getUnassignedAnimals();
    };

    $scope.selectAllAnimals = function(isSelected,animalsArray){
        animalsArray.forEach(function (val) {
            val.selected = isSelected;
          });
    };

    $scope.allSpecies = [];

    animalsService.getSpeciesList()
        .then(function (data) {
            $scope.allSpecies = data;
        },function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });

    $scope.assignAnimalsToProcedure = function () {
        var selectedAnimals = $filter('filter')($scope.unassignedAnimals, { selected: true });
        var animalsArray = [];
        angular.forEach(selectedAnimals, function (data, index) {
            animalsArray.push(data.id);
        });
        studyService.assignAnimalsToProcedure(100, $scope.clinicalProcedure.id, animalsArray)
        .then(function (data) {
            $scope.clinicalProcedure = data;
            toaster.pop('success', 'Success', 'Animals assigned successfully');
            $scope.procAnimals = true;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.unassignAnimalsFromProcedure = function (animalId) {
        studyService.unassignAnimalsFromProcedureById(100, $scope.clinicalProcedure.id, animalId)
        .then(function (data) {
            $scope.clinicalProcedure = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.createSchedule = function (sheduleDate) {
        var schedule = {
            "scheduleDate": moment(sheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
            "status": "SCHEDULED"
        };
        studyService.scheduleProcedure(100, $scope.clinicalProcedure.id, schedule)
        .then(function (data) {
            $scope.clinicalProcedure = data;
            toaster.pop('success', 'Success', 'Procedure created successfully');
            return true;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
            return false;
        });
        return true;
    };

    $scope.procedureSource = [];


    $scope.eventRender = function (event, element, view) {
        element.attr({
            'tooltip': event.title,
            'tooltip-append-to-body': true
        });
        $compile(element)($scope);
    };

    $scope.dayChanged = function (event, element) {
        console.log(element);
    };

    $scope.changeView = function (view, calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        $scope.displayDate();
    };

    $scope.nextDay = function (calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('next');
        $scope.displayDate();
    };

    $scope.prevDay = function (calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('prev');
        $scope.displayDate();
    };

    $scope.displayDate = function () {
        var selectedDate, view, format;
        if (uiCalendarConfig.calendars['procedureCalendar']) {
            selectedDate = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getDate');
            view = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getView');
        } else {
            selectedDate = new Date();
        }
        if (view && view.title) {
            $scope.selectedDay = view.title;
        } else {
            $scope.selectedDay = moment(selectedDate).format("MMMM YYYY");
        }
    };

    $scope.isProcRendered = false;

    $scope.renderCalendar = function () {
        if(!$scope.isProcRendered){
            $timeout(function () {
                $scope.displayDate();
                uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('render');
                uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('removeEvents');
                mapProcedureSchedules();
            }, 0);
            $scope.isProcRendered = true;
        }
    };

    $scope.deleteSchedule = function (sheduleId, index) {
        studyService.deleteSchedule(100, $scope.clinicalProcedure.id, sheduleId)
        .then(function (data) {
            //$scope.studyProcedure = data;
            toaster.pop('success', 'Deleted', 'Schedule deleted successfully');
            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('removeEvents', sheduleId);
            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('render');
            $scope.clinicalProcedure.procedureSchedules.splice(index,1);
            mapProcedureSchedules();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.procedureSource = [];

    function mapProcedureSchedules() {
        $scope.procedureSource = [];

        angular.forEach($scope.clinicalProcedure.procedureSchedules, function (data, index) {
            var eventData = {
                id:data.id,
                title: 'SCHEDULE - '+ data.status,
                start: moment.utc(data.scheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
                status: data.status
            };
            $scope.procedureSource.push(eventData);

            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('renderEvent', eventData, true);
        });
    }

    $scope.showScheduleListView = function () {
        $scope.scheduleCalendarView = false;
    };

    $scope.showScheduleCalendarView = function () {
        $scope.scheduleCalendarView = true;
    };

    $scope.convertUtcDate = function(date,format){
        return moment.utc(date).format(format);
    };

    $scope.changeProcedureStatus = function (procedurestatus) {
        studyService.changeProcedureStatus(100, $scope.clinicalProcedure.id, procedurestatus)
      .then(function (data) {
          $scope.clinicalProcedure = data;
          toaster.pop('success', 'Status', 'Status updated successfully');
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.checkAnimalExistsinProcedure = function(animal){

        return typeof $scope.clinicalProcedure.animals !== 'undefined' && $scope.clinicalProcedure.animals !== null && $filter('filter')($scope.clinicalProcedure.animals, { id: animal.id }).length <= 0;
    };

    window.scope = $scope;
});