angular.module('protocol', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate']);

angular.module('protocol').config(function ($stateProvider) {

    $stateProvider.state('protocol-list', {
        url: '/protocol',
        templateUrl: 'protocol/partial/protocol-list/protocol-list.html',
        data: { pageTitle: 'Protocols' }
    })
    .state('protocolDetails', {
        url: '/protocolDetails/:id/:type',
        templateUrl: 'protocol/partial/protocolDetails/protocolDetails.html',
        data: { pageTitle: 'Protocol Details' }
    });
    /* Add New States Above */
});

