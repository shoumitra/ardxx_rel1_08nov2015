angular.module('ardxx').factory('configService', function () {

	var configService = {};

	var myConfig = {
		"url": "@@url",
		"port": "@@port",
		"buildEnv": "@@buildEnv"
	};

	configService.getHostName = myConfig.url + ':' + myConfig.port + '/';
	
	configService.buildEnv = myConfig.buildEnv;

	return configService;
});