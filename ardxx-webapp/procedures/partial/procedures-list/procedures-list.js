angular.module('procedures').controller('ProceduresListCtrl', function ($scope, studyService) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.procedures = [{
        id: 1,
        title: 'Procedure 01 - Schedure 1',
        start: new Date(),
        procedure: 'Procedure 01',
        study: 'S01'
    }, {
        id: 2,
        title: 'Procedure 02',
        start: new Date(y, m, d - 3, 16, 0),
        procedure: 'Procedure 02',
        study: 'S01'
    }, {
        id: 3,
        title: 'Procedure 03',
        start: new Date(y, m, d + 4, 16, 0),
        allDay: false,
        procedure: 'Procedure 03',
        study: 'S02'
    }, {
        id: 4,
        title: 'Procedure 03',
        start: new Date(y, m, d + 1, 19, 0),
        allDay: false,
        procedure: 'Procedure 03',
        study: 'S02'
    }, {
        id: 6,
        title: 'Procedure 04',
        start: new Date(y, m - 1, d, 19, 0),
        allDay: false,
        procedure: 'Procedure 04',
        study: 'S03'
    }, {
        id: 7,
        title: 'Procedure 06',
        start: new Date(y, m + 1, d, 19, 0),
        allDay: false,
        procedure: 'Procedure 06',
        study: 'S03'
    }];

    window.scope = $scope;
});
