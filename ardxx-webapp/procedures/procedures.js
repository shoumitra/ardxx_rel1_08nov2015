angular.module('procedures', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('procedures').config(function($stateProvider) {

    $stateProvider.state('procedures-list', {
        url: '/procedure-list',
        templateUrl: 'procedures/partial/procedures-list/procedures-list.html',
        data: { pageTitle: 'Procedures' }
    });
    /* Add New States Above */

});

