angular.module('login').controller('LogoutCtrl',function($scope,$rootScope,$state,authenticationService){
	authenticationService.clearCredentials();
	$rootScope.userAuthenticated = false;
	$state.go('login',{});
});