angular.module('login', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('login').config(function($stateProvider) {

    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'login/partial/login/login.html',
        data : { pageTitle: 'Login' }
    });
    $stateProvider.state('logout', {
        url: 'logout',
        templateUrl: 'login/partial/logout/logout.html',
        data : { pageTitle: 'Logout' }
    });
    /* Add New States Above */

});

