angular.module('ardxx').factory('configService', function () {

	var configService = {};

	var myConfig = {
		"url": "http://localhost",
		"port": "8080",
		"buildEnv": "DEV"
	};

	configService.getHostName = myConfig.url + ':' + myConfig.port + '/';
	
	configService.buildEnv = myConfig.buildEnv;

	return configService;
});