package com.tn.commons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TnCommonsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TnCommonsApplication.class, args);
    }
}
